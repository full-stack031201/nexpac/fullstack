/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80030 (8.0.30)
 Source Host           : localhost:3306
 Source Schema         : db_nexpac

 Target Server Type    : MySQL
 Target Server Version : 80030 (8.0.30)
 File Encoding         : 65001

 Date: 18/06/2024 13:00:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for about_us
-- ----------------------------
DROP TABLE IF EXISTS `about_us`;
CREATE TABLE `about_us`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `section1_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section1_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section1_desc1` longtext CHARACTER SET utf8mb4 NULL,
  `section1_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section2_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section2_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section3_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section3_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section4_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section4_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section5_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of about_us
-- ----------------------------
INSERT INTO `about_us` VALUES (1, '<p>OVERVIEW</p>', '<p>The local food industry in Indonesia has long struggled with a lack of<br />\r\naffordable, high quality, food grade processing and packaging<br />\r\nsolutions. <strong>Nexpac</strong> is an Indonesian food and beverage</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Based in the Greater Jakarta Area, Nexpac is part of Sansico Group,<br />\r\nan diversified international group with strong Asian presence that<br />\r\nwas founded in 1977.</p>', NULL, 'https://nexpac.id/assets/images/about-us/overview.png', '<p>VISION</p>', '<p>To be the next system supplier of choice for<br />\r\nthe food &amp; beverage industry in Indonesia</p>', '<p>MISION</p>', '<p>We provide packaging solution and service for<br />\r\nfood &amp; beverage industry with the spirit of partnership</p>', '<p>CERTIFICATIONS</p>', '<p>We are a fully certified company</p>', 'https://nexpac.id/assets/images/home/2024-03-22-09-03-22.png', '2024-06-17 11:33:31', '2024-06-18 01:31:10');

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 'admin', '$2y$10$I4mwEVkgT9sS9cM/WV0m2OABgWQARaAeYMc3NLXRrLkq8KldC8vD.', 1, NULL, NULL);
INSERT INTO `admins` VALUES (3, 'admin123', '$2y$10$pyYXvd8Iro9K8bSkcVBHHOAnzWbtY0XWgt6QvuPfwjGrMzuE5HLNO', 1, '2024-06-17 12:50:17', '2024-06-17 13:31:45');

-- ----------------------------
-- Table structure for application_category
-- ----------------------------
DROP TABLE IF EXISTS `application_category`;
CREATE TABLE `application_category`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `header_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `title_heading` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `desc_heading` longtext CHARACTER SET utf8mb4 NULL,
  `desc1_heading` longtext CHARACTER SET utf8mb4 NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of application_category
-- ----------------------------
INSERT INTO `application_category` VALUES (1, 'https://nexpac.id/assets/images/packaging/Header Aseptic Packaging.png', 'ASEPTIC PACKAGING', '\r\nNexpac Aseptic Packaging is specially made<br />to be a packaging that protects the quality of food<br />and beverages inside packaging until delivered to consumers.<br /><br />The aseptic packaging is used for various food products and beverages,<br />including milk, juice, tea, coconut cream, pudding and several others.', NULL, '2024-06-17 12:02:54', '2024-06-17 21:25:24');
INSERT INTO `application_category` VALUES (2, 'https://nexpac.id/assets/images/packaging/Banner.jpg', 'PAPER PACKAGING', 'Nexpac is a food packaging company with aseptic standard<br />for packaging lamination in indonesia, that is committed to<br />customizable with environmentally safe packaging design and material.', NULL, '2024-06-17 12:06:04', '2024-06-17 21:25:10');

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications`  (
  `application_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `application_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `application_spec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `application_about` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `application_category` bigint NOT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`application_id`) USING BTREE,
  INDEX `category_foreign_key`(`application_category` ASC) USING BTREE,
  CONSTRAINT `category_foreign_key` FOREIGN KEY (`application_category`) REFERENCES `application_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of applications
-- ----------------------------
INSERT INTO `applications` VALUES (1, 'https://nexpac.id/assets/images/packaging/Coconut Cream.jpg', 'COCONUT CREAM', 'Kemasan Segitiga 65ml', NULL, 1, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (2, 'https://nexpac.id/assets/images/packaging/Milk.jpg', 'MILK', 'Kemasan Kotak 125ml - 250ml', NULL, 1, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (3, 'https://nexpac.id/assets/images/packaging/Tea.jpg', 'TEA', 'Kemasan Kotak 125ml - 250ml', NULL, 1, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (4, 'https://nexpac.id/assets/images/packaging/Juice.jpg', 'JUICE', 'Kemasan Kotak 125ml - 250ml', NULL, 1, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (5, 'https://nexpac.id/assets/images/packaging/warm food.jpg', 'WARM FOOD', NULL, 'Paper packaging is heat resistance to the freshly cook food.\r\n<br>\r\nThis advantage allows you to eat food while it is still warm.\r\n<br>\r\nNexpac use aseptic standards for packaging lamination\r\n<br>\r\nso that you can immediately consume food in a warm state.', 2, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (6, 'https://nexpac.id/assets/images/packaging/Microwave.jpg', 'MICROWAVE SAFE', NULL, 'The heat-retaining of the paper allow you to warm\r\n<br>\r\nthe food in the microwave. Nexpac is microwaveable\r\n<br>\r\nfood packaging.', 2, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (7, 'https://nexpac.id/assets/images/packaging/Cold Drink or Ice Cream.jpg', 'COLD DRINK<br>OR ICE CREAM', NULL, 'Paper Cup Packaging is simple and easy to carry.\r\n<br>\r\nNexpac Paper cups in addition for cold drinks\r\n<br>\r\ncan also be used for ice cream and prevent flavor\r\n<br>\r\nchanges because of wet packaging.', 2, NULL, NULL, NULL);
INSERT INTO `applications` VALUES (8, 'https://nexpac.id/assets/images/packaging/soupy food.jpg', 'HOT DRINK<br>OR SOUPY FOOD', NULL, 'The heat-resistant of nexpac packaging can\r\n<br>\r\nmake easier to pack hot coffee and also soupy foods.', 2, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for certifications
-- ----------------------------
DROP TABLE IF EXISTS `certifications`;
CREATE TABLE `certifications`  (
  `certification_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `certification_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `certification_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`certification_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of certifications
-- ----------------------------
INSERT INTO `certifications` VALUES (2, 'Certification 0', 'socialcompliance_1.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (3, 'Certification 1', 'forestry.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (4, 'Certification 2', 'environmental.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (5, 'Certification 3', 'quality.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (6, 'Certification 4', 'energy.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (7, 'Certification 5', 'socialcompliance_2.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (8, 'Certification 6', 'forestry1.png', NULL, NULL, NULL);
INSERT INTO `certifications` VALUES (9, 'Certification 7', 'socialcompliance_3.png', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `logo_sansico` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `header_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `maps` longtext CHARACTER SET utf8mb4 NULL,
  `title_section1` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `perusahaan` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `alamat` longtext CHARACTER SET utf8mb4 NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `telp` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `fb` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `ig` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `linkedin` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES (1, 'https://nexpac.id/assets/images/Logo Sansico.png', 'https://nexpac.id/assets/images/contact/Contact Us.jpg', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2873136622698!2d106.78246831435057!3d-6.225797662705745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f131ae27f3cd%3A0xc19276eedb276f0d!2sNexpac%20Aseptik%20Indonesia!5e0!3m2!1sid!2sid!4', 'FIND US', 'Nexpac Aseptik Indonesia', 'Grand ITC Permata Hijau, Blok Diamond 11-12, Jl Arteri Permata Hijau No. 11 RT 11/RW 10 Kec. Kebayoran Lama - Jakarta Selatan 12210, Indonesia', 'support@nexpac.id', '(021) 5366380', 'https://www.facebook.com/NexpacFoodPackaging', 'https://www.instagram.com/nexpac.id/', 'https://www.linkedin.com/company/nexpacaseptikindonesia/about/', '2024-06-17 10:37:15', '2024-06-17 12:31:27');

-- ----------------------------
-- Table structure for contact_us
-- ----------------------------
DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us`  (
  `contact_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`contact_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contact_us
-- ----------------------------
INSERT INTO `contact_us` VALUES (1, 'Shiota', 'Zero', 'shiozero03@gmail.com', '0875467899', 'Fameran', 'Owner', NULL, '2023-03-19 00:25:33', '2023-03-19 00:25:33');
INSERT INTO `contact_us` VALUES (2, 'njjnj', 'njnjn', 'jnjnjn@njjn.jnjn', '899', 'jjnj', 'njjn', NULL, '2023-04-06 09:42:22', '2023-04-06 09:42:22');
INSERT INTO `contact_us` VALUES (3, 'Shiota', 'Zero', 'shiozero03@gmail.com', '0828188189', 'Fameran', 'Owner', NULL, '2023-04-06 19:29:47', '2023-04-06 19:29:47');
INSERT INTO `contact_us` VALUES (4, 'Test', 'Oke', 'mhdjan@jamm.com', '09099', 'kmkm', 'kmkm', NULL, '2023-04-07 01:15:25', '2023-04-07 01:15:25');
INSERT INTO `contact_us` VALUES (5, 'Haha', 'Okelah', 'Fix@gmail.com', '80909', 'mkmkm', 'kmkm', NULL, '2023-04-07 01:16:30', '2023-04-07 01:16:30');
INSERT INTO `contact_us` VALUES (6, 'Shiota', 'Zero', 'shiotarafli1@gmail.com', '082275713049', 'Fameran', 'OWNER', NULL, '2023-04-07 04:32:29', '2023-04-07 04:32:29');
INSERT INTO `contact_us` VALUES (7, 'shiota', 'zero', 'shiotazero03@gmail.com', '082275713049', 'Fameran', 'Owner', NULL, '2023-04-07 04:33:38', '2023-04-07 04:33:38');
INSERT INTO `contact_us` VALUES (8, 'Shiota', 'Zero', 'shiotazero03@gmail.com', '082275713049', 'Fameran', 'Owner', NULL, '2023-04-07 04:34:53', '2023-04-07 04:34:53');
INSERT INTO `contact_us` VALUES (9, 'Shiota', 'Zero', 'shiotazero03@gmail.com', '082275713049', 'Fameran', 'Owner', NULL, '2023-04-07 04:37:17', '2023-04-07 04:37:17');
INSERT INTO `contact_us` VALUES (10, 'Shiota', 'Zero', 'shiozero@gmail.com', '091898', 'njnjnk', 'Manambin', NULL, '2023-04-07 06:15:22', '2023-04-07 06:15:22');
INSERT INTO `contact_us` VALUES (11, 'Shiota', 'Zero', 'nidanuraffisah.nna@gmail.com', '082275713049', 'Fameran', 'Owner', NULL, '2023-04-08 05:20:14', '2023-04-08 05:20:14');
INSERT INTO `contact_us` VALUES (12, 'gita', 'sasfa', 'gitanu98@gmail.com', '-10', 'sdasfas', 'afasfasfszsczx', NULL, '2023-05-08 01:33:35', '2023-05-08 01:33:35');
INSERT INTO `contact_us` VALUES (13, 'gita', 'sasfa', 'gitanu98@gmail.com', '-10', 'sdasfas', 'afasfasfszsczx', NULL, '2023-05-08 01:33:41', '2023-05-08 01:33:41');
INSERT INTO `contact_us` VALUES (14, 'fn', 'test', 'fajarnovianto@usa.com', '08392108390', 'test', 'test', NULL, '2023-05-09 04:41:02', '2023-05-09 04:41:02');
INSERT INTO `contact_us` VALUES (15, 'fn', 'test', 'fajarnovianto@usa.com', '8392108393', 'test', 'test', NULL, '2023-05-09 04:41:12', '2023-05-09 04:41:12');
INSERT INTO `contact_us` VALUES (16, 'fn', 'test', 'fajarnovianto@usa.com', '8392108393', 'test', 'test', NULL, '2023-05-09 04:41:13', '2023-05-09 04:41:13');
INSERT INTO `contact_us` VALUES (17, 'fnsaxdasd', 'test', 'fajarnovianto@usa.com', '8392108393', 'test', 'test2321', NULL, '2023-05-09 04:41:19', '2023-05-09 04:41:19');
INSERT INTO `contact_us` VALUES (18, 'fnsaxdasd', 'test', 'fajarnovianto@usa.com', '8392108393', 'test', 'test2321', NULL, '2023-05-09 04:41:20', '2023-05-09 04:41:20');
INSERT INTO `contact_us` VALUES (19, 'fnsaxdasd', 'testsad', 'fajarnovianto@usa.com', '8392108393', 'test', 'test2321', NULL, '2023-05-09 04:41:23', '2023-05-09 04:41:23');
INSERT INTO `contact_us` VALUES (20, 'asdasda', 'dsadsadsa', 'fnfn.fajarnovianto@gmail.com', '123123123', 'asdsad', 'sadsadas', NULL, '2023-05-09 04:41:40', '2023-05-09 04:41:40');
INSERT INTO `contact_us` VALUES (21, 'asdasda', 'dsadsadsa', 'fnfn.fajarnovianto@gmail.com', '123123123', 'asdsad', 'sadsadas', NULL, '2023-05-09 04:42:39', '2023-05-09 04:42:39');
INSERT INTO `contact_us` VALUES (22, 'asdasda', 'dsadsadsa', 'fnfn.fajarnovianto@gmail.com', '123123123', 'asdsad', 'sadsadas', NULL, '2023-05-09 04:42:42', '2023-05-09 04:42:42');
INSERT INTO `contact_us` VALUES (23, 'asdasda', 'dsadsadsa', 'fnfn.fajarnovianto@gmail.com', '123123123', 'asdsad', 'sadsadas', NULL, '2023-05-09 04:42:56', '2023-05-09 04:42:56');
INSERT INTO `contact_us` VALUES (24, 'fahar', 'reare', 'fajarnovianto@usa.com', '08998231083', 'fhajsh', 'adsadas', NULL, '2023-05-09 04:46:57', '2023-05-09 04:46:57');
INSERT INTO `contact_us` VALUES (25, 'sadsa', 'asda', 'sad@gmail.com', '9812930180293', 'fhajs', 'fajks', NULL, '2023-05-09 04:47:26', '2023-05-09 04:47:26');
INSERT INTO `contact_us` VALUES (26, 'sadsa', 'asda', 'sad@gmail.com', '9812930180293', 'fhajs', 'fajks', NULL, '2023-05-09 04:47:28', '2023-05-09 04:47:28');
INSERT INTO `contact_us` VALUES (27, 'sadsa', 'asda', 'sad@gmail.com', '9812930180293', 'fhajs', 'fajks', NULL, '2023-05-09 04:47:29', '2023-05-09 04:47:29');
INSERT INTO `contact_us` VALUES (28, 'sadsa', 'asda', 'sad@gmail.com', '9812930180293', 'fhajs', 'fajks', NULL, '2023-05-09 04:47:29', '2023-05-09 04:47:29');
INSERT INTO `contact_us` VALUES (29, 'sadsa', 'asda', 'sad@gmail.com', '9812930180293', 'fhajs', 'fajks', NULL, '2023-05-09 04:47:30', '2023-05-09 04:47:30');
INSERT INTO `contact_us` VALUES (30, 'asdas', 'asdasd', 'fajarnovianto@usa.com', 'test', 'fsadsa', 'tasdsa', NULL, '2023-05-09 04:51:00', '2023-05-09 04:51:00');
INSERT INTO `contact_us` VALUES (31, 'qwesd', 'sadasd', 'fajarnovianto@usa.com', '1231231', 'afsa', 'fasdas', NULL, '2023-05-09 04:53:09', '2023-05-09 04:53:09');
INSERT INTO `contact_us` VALUES (32, 'wqedwqdd', 'dsadsa', 'fajarnovianto@usa.com', '08950950898192', 'test company', 'position hdr', NULL, '2023-05-09 04:55:39', '2023-05-09 04:55:39');
INSERT INTO `contact_us` VALUES (33, 'wqedwqdd', 'dsadsa', 'fajarnovianto@usa.com', '08950950898192', 'test company', 'position hdr', NULL, '2023-05-09 04:56:12', '2023-05-09 04:56:12');
INSERT INTO `contact_us` VALUES (34, 'wqedwqdd', 'dsadsa', 'fajarnovianto@usa.com', '08950950898192', 'test company', 'position hdr', NULL, '2023-05-09 04:56:56', '2023-05-09 04:56:56');
INSERT INTO `contact_us` VALUES (35, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 05:09:24', '2023-05-09 05:09:24');
INSERT INTO `contact_us` VALUES (36, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 05:09:26', '2023-05-09 05:09:26');
INSERT INTO `contact_us` VALUES (37, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 05:09:29', '2023-05-09 05:09:29');
INSERT INTO `contact_us` VALUES (38, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 05:09:29', '2023-05-09 05:09:29');
INSERT INTO `contact_us` VALUES (39, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 05:09:30', '2023-05-09 05:09:30');
INSERT INTO `contact_us` VALUES (40, 'fasad', 'fasfdsa', 'dsadsa@fdsadf.com', '123123', 'frqasda', 'fsadsadda', NULL, '2023-05-09 11:49:38', '2023-05-09 11:49:38');
INSERT INTO `contact_us` VALUES (41, 'test', 'test', 'fajarnovianto@usa.com', '1231321', 'fasda', 'dsadsa', NULL, '2023-05-09 12:04:13', '2023-05-09 12:04:13');
INSERT INTO `contact_us` VALUES (42, 'qweqe', 'qwewqe', 'fajarnovianto@usa.com', '11782738197', 'test', 'testt', NULL, '2023-05-09 12:07:51', '2023-05-09 12:07:51');
INSERT INTO `contact_us` VALUES (43, 'sdasds', 'sadsa', 'fajarnovianto@usa.com', '12312', 'fasfsa', 'sfafas', NULL, '2023-05-09 12:13:26', '2023-05-09 12:13:26');
INSERT INTO `contact_us` VALUES (44, 'asdasd', 'asdasds', 'fajarnovianto@usa.com', '1231321', 'fafsa', 'dsadsadsa', NULL, '2023-05-09 12:17:39', '2023-05-09 12:17:39');
INSERT INTO `contact_us` VALUES (45, 'zcvSDVsD', 'dsvsdv', 'ZVdssv', '54346', 'dsfbgfdbz', 'dzfbdzfbzdfbh', NULL, '2023-05-19 02:47:39', '2023-05-19 02:47:39');
INSERT INTO `contact_us` VALUES (46, 'gjfg', 'fhfg', 'fghfg', '56547568', 'fdhfgh', 'gfhfghfg', NULL, '2023-05-22 06:33:40', '2023-05-22 06:33:40');
INSERT INTO `contact_us` VALUES (47, 'gjfg', 'fhfg', 'fghfg', '56547568', 'fdhfgh', 'gfhfghfg', NULL, '2023-05-22 06:33:45', '2023-05-22 06:33:45');
INSERT INTO `contact_us` VALUES (48, 'gjfg', 'fhfg', 'shiningbox1@gmail.com', '56547568', 'fdhfgh', 'gfhfghfg', NULL, '2023-05-22 06:34:05', '2023-05-22 06:34:05');
INSERT INTO `contact_us` VALUES (49, 'gita', 'gfdsg', 'gitanu98@gmail.com', '543465376', 'dgadfgz', 'zdbhfdhbzd', NULL, '2023-05-29 07:53:10', '2023-05-29 07:53:10');
INSERT INTO `contact_us` VALUES (50, 'fajar', 'novianto', 'fajarnovianto@usa.com', '085697760428', 'test', 'jaksa', NULL, '2023-06-03 08:25:45', '2023-06-03 08:25:45');
INSERT INTO `contact_us` VALUES (51, 'fajar', 'novianto', 'fajarnovianto@usa.com', '085697760428', 'adhnjsl', 'hfjsladsa', NULL, '2023-06-03 08:26:05', '2023-06-03 08:26:05');
INSERT INTO `contact_us` VALUES (52, 'sdfsdg', 'afgsd', 'sosmedigpklaten@gmail.com', '1234425346', 'fdsdgsdf', 'dvsdvsdc', NULL, '2023-06-08 09:16:58', '2023-06-08 09:16:58');
INSERT INTO `contact_us` VALUES (53, 'fajar', 'novianto', 'fnfn.fajarnovianto@gmail.com', '085697760428', 'test', 'test', NULL, '2023-07-03 23:19:43', '2023-07-03 23:19:43');
INSERT INTO `contact_us` VALUES (54, 'Fajar', 'Novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'test', NULL, '2023-07-03 23:20:54', '2023-07-03 23:20:54');
INSERT INTO `contact_us` VALUES (55, 'fajar', 'noviantgo', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'testt', NULL, '2023-07-03 23:24:13', '2023-07-03 23:24:13');
INSERT INTO `contact_us` VALUES (56, 'fajar', 'noviantgo', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'testt', NULL, '2023-07-03 23:24:21', '2023-07-03 23:24:21');
INSERT INTO `contact_us` VALUES (57, 'fajar', 'noviantgo', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'testt', NULL, '2023-07-03 23:24:23', '2023-07-03 23:24:23');
INSERT INTO `contact_us` VALUES (58, 'fajar', 'novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'testt', NULL, '2023-07-03 23:24:40', '2023-07-03 23:24:40');
INSERT INTO `contact_us` VALUES (59, 'fajar', 'novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'testttt', 'testtttt', NULL, '2023-07-03 23:31:03', '2023-07-03 23:31:03');
INSERT INTO `contact_us` VALUES (60, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '1242352', 'afsd', 'AFASD', NULL, '2023-07-10 03:01:35', '2023-07-10 03:01:35');
INSERT INTO `contact_us` VALUES (61, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '1242352', 'afsd', 'AFASD', NULL, '2023-07-10 03:01:42', '2023-07-10 03:01:42');
INSERT INTO `contact_us` VALUES (62, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '124235254426', 'afsdsdgsd sdgdsf', 'AFASD sdgsdg', NULL, '2023-07-10 03:01:50', '2023-07-10 03:01:50');
INSERT INTO `contact_us` VALUES (63, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '124235254426', 'afsdsdgsd sdgdsf', 'AFASD sdgsdg', NULL, '2023-07-10 03:01:50', '2023-07-10 03:01:50');
INSERT INTO `contact_us` VALUES (64, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '124235254426', 'afsdsdgsd sdgdsf', 'AFASD sdgsdg', NULL, '2023-07-10 03:01:50', '2023-07-10 03:01:50');
INSERT INTO `contact_us` VALUES (65, 'zCsdc', 'ascfasdfc', 'sansicodigitalmarketing@gmail.com', '124235254426', 'afsdsdgsd sdgdsf', 'AFASD sdgsdg', NULL, '2023-07-10 03:01:51', '2023-07-10 03:01:51');
INSERT INTO `contact_us` VALUES (66, 'sdgfdsg', 'sdvgsdG', 'sansicodigitalmarketing@gmail.com', '3513245', 'sdgsdg', 'AFSSDG', NULL, '2023-07-10 03:02:05', '2023-07-10 03:02:05');
INSERT INTO `contact_us` VALUES (67, 'sdgfdsg', 'sdvgsdG', 'sansicodigitalmarketing@gmail.com', '3513245', 'sdgsdg', 'AFSSDG', NULL, '2023-07-10 03:02:07', '2023-07-10 03:02:07');
INSERT INTO `contact_us` VALUES (68, 'sdgfdsg', 'sdvgsdG', 'sansicodigitalmarketing@gmail.com', '3513245', 'sdgsdg', 'AFSSDG', NULL, '2023-07-10 03:02:08', '2023-07-10 03:02:08');
INSERT INTO `contact_us` VALUES (69, 'sdgfdsg', 'sdvgsdG', 'sansicodigitalmarketing@gmail.com', '3513245', 'sdgsdg', 'AFSSDG', NULL, '2023-07-10 03:02:08', '2023-07-10 03:02:08');
INSERT INTO `contact_us` VALUES (70, 'fajar', 'nvoaaod', 'fajarnovianto.fnfn@gmail.com', '6385697760428', 'sjdkajd', 'nhfdljasds', NULL, '2023-07-10 03:04:01', '2023-07-10 03:04:01');
INSERT INTO `contact_us` VALUES (71, 'dfgSG', 'DGADFH', 'sansicodigitalmarketing@gmail.com', '465645', 'DFHZDFH', 'ZDHFDHZDF', NULL, '2023-07-11 06:45:41', '2023-07-11 06:45:41');
INSERT INTO `contact_us` VALUES (72, 'dfgSG', 'DGADFH', 'sansicodigitalmarketing@gmail.com', '465645', 'DFHZDFH', 'ZDHFDHZDF', NULL, '2023-07-11 06:45:44', '2023-07-11 06:45:44');
INSERT INTO `contact_us` VALUES (73, 'dfgSG', 'DGADFH', 'sansicodigitalmarketing@gmail.com', '465645', 'DFHZDFH', 'ZDHFDHZDF', NULL, '2023-07-11 06:46:01', '2023-07-11 06:46:01');
INSERT INTO `contact_us` VALUES (74, 'fajar', 'novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'ajikajsdklanjlk', 'anksdlandsa', NULL, '2023-07-12 08:14:50', '2023-07-12 08:14:50');
INSERT INTO `contact_us` VALUES (75, 'fajar', 'novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'ajikajsdklanjlk', 'anksdlandsa', NULL, '2023-07-12 08:14:57', '2023-07-12 08:14:57');
INSERT INTO `contact_us` VALUES (76, 'fajar', 'novianto', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'ajikajsdklanjlk', 'anksdlandsa', NULL, '2023-07-12 08:14:57', '2023-07-12 08:14:57');
INSERT INTO `contact_us` VALUES (77, 'gita', 'ghjkjk', 'sansicodigitalmarketing@gmail.com', '12345678', 'dfghjkl', 'sdfghjnk', NULL, '2023-07-14 01:39:28', '2023-07-14 01:39:28');
INSERT INTO `contact_us` VALUES (78, 'Network', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'fajsidjai', 'jhfikasldaj;sd', NULL, '2023-07-14 02:43:40', '2023-07-14 02:43:40');
INSERT INTO `contact_us` VALUES (79, 'fajar', 'novianto', 'fajarnovianto@usa.com', '98321098320918', 'jfkajsl', 'jlkdfasda', NULL, '2023-07-14 02:45:43', '2023-07-14 02:45:43');
INSERT INTO `contact_us` VALUES (80, 'fasfasd', 'gafsadsfa', 'fajarnovianto@usa.com', '085785758798', 'jajikshdjaiohd', 'ajsdhaldslad asda', NULL, '2023-07-14 02:49:19', '2023-07-14 02:49:19');
INSERT INTO `contact_us` VALUES (81, 'gita', 'dvsd', 'sansicodigitalmarketing@gmail.com', '12456789', 'sdgfhgjhkj', 'zdhgxfjhgkj', NULL, '2023-07-17 07:44:02', '2023-07-17 07:44:02');
INSERT INTO `contact_us` VALUES (82, 'dfhd', 'dfhdf', 'sansicodigitalmarketing@gmail.com', '1235678', 'sDzgfxhg', 'sdgfhgjhj', NULL, '2023-07-20 01:52:41', '2023-07-20 01:52:41');
INSERT INTO `contact_us` VALUES (83, 'dsadsa', 'Computer', 'fajarnovianto.fnfn@gmail.com', '085697760428', 'test', 'test', NULL, '2023-07-22 03:27:22', '2023-07-22 03:27:22');
INSERT INTO `contact_us` VALUES (84, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 03:46:42', '2023-07-22 03:46:42');
INSERT INTO `contact_us` VALUES (85, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 03:59:45', '2023-07-22 03:59:45');
INSERT INTO `contact_us` VALUES (86, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 04:00:17', '2023-07-22 04:00:17');
INSERT INTO `contact_us` VALUES (87, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 04:00:18', '2023-07-22 04:00:18');
INSERT INTO `contact_us` VALUES (88, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 04:00:19', '2023-07-22 04:00:19');
INSERT INTO `contact_us` VALUES (89, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 04:00:19', '2023-07-22 04:00:19');
INSERT INTO `contact_us` VALUES (90, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'test', NULL, '2023-07-22 04:00:19', '2023-07-22 04:00:19');
INSERT INTO `contact_us` VALUES (91, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'testsad', NULL, '2023-07-22 04:00:21', '2023-07-22 04:00:21');
INSERT INTO `contact_us` VALUES (92, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'testsad', NULL, '2023-07-22 04:00:21', '2023-07-22 04:00:21');
INSERT INTO `contact_us` VALUES (93, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'testsad', NULL, '2023-07-22 04:00:21', '2023-07-22 04:00:21');
INSERT INTO `contact_us` VALUES (94, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'testsad', NULL, '2023-07-22 04:01:49', '2023-07-22 04:01:49');
INSERT INTO `contact_us` VALUES (95, 'NF', 'Computer', 'fajarnovianto@usa.com', '085697760428', 'test', 'testsad', NULL, '2023-07-22 04:01:50', '2023-07-22 04:01:50');
INSERT INTO `contact_us` VALUES (96, 'NF', 'Computer', 'fajarnovianto@usa.com', '08983291893', 'jdkaj', 'sadadas', NULL, '2023-07-22 04:06:14', '2023-07-22 04:06:14');
INSERT INTO `contact_us` VALUES (97, 'NF', 'Computer', 'fajarnovianto@usa.com', '08983291893', 'jdkaj', 'sadadas', NULL, '2023-07-22 04:06:16', '2023-07-22 04:06:16');
INSERT INTO `contact_us` VALUES (98, 'NF', 'Computer', 'fajarnovianto@usa.com', '08983291893', 'jdkaj', 'sadadas', NULL, '2023-07-22 04:06:16', '2023-07-22 04:06:16');
INSERT INTO `contact_us` VALUES (99, 'NF', 'Computer', 'fajarnovianto@usa.com', '08983291893', 'jdkaj', 'sadadas', NULL, '2023-07-22 04:06:28', '2023-07-22 04:06:28');
INSERT INTO `contact_us` VALUES (100, 'NF', 'Computer', 'fajarnovianto@usa.com', '08983291893', 'jdkaj', 'sadadas', NULL, '2023-07-22 04:06:28', '2023-07-22 04:06:28');
INSERT INTO `contact_us` VALUES (101, 'dsadsad', 'sadsa', 'fajarnovianto@usa.com', '1231231', 'asda', 'wdesa', NULL, '2023-07-22 04:07:53', '2023-07-22 04:07:53');
INSERT INTO `contact_us` VALUES (102, 'dsadsad', 'sadsa', 'fajarnovianto@usa.com', '1231231', 'asda', 'wdesa', NULL, '2023-07-22 04:07:54', '2023-07-22 04:07:54');
INSERT INTO `contact_us` VALUES (103, 'dsadsad', 'sadsa', 'fajarnovianto@usa.com', '1231231', 'asda', 'wdesa', NULL, '2023-07-22 04:07:55', '2023-07-22 04:07:55');
INSERT INTO `contact_us` VALUES (104, 'dsadsad', 'sadsa', 'fajarnovianto@usa.com', '1231231', 'asda', 'wdesa', NULL, '2023-07-22 04:07:55', '2023-07-22 04:07:55');
INSERT INTO `contact_us` VALUES (105, 'dsadsad', 'sadsa', 'fajarnovianto@usa.com', '1231231', 'asda', 'wdesa', NULL, '2023-07-22 04:07:55', '2023-07-22 04:07:55');
INSERT INTO `contact_us` VALUES (106, 'fasfasda', 'sfsadsadsad', 'fajarnovianto@usa.com', '12321321321', 'fasdassadsa', 'dsdadsadsadsa', NULL, '2023-07-22 15:52:30', '2023-07-22 15:52:30');
INSERT INTO `contact_us` VALUES (107, 'fasfasda', 'sfsadsadsad', 'fajarnovianto@usa.com', '12321321321', 'fasdassadsa', 'dsdadsadsadsa', NULL, '2023-07-22 15:53:12', '2023-07-22 15:53:12');
INSERT INTO `contact_us` VALUES (108, 'fasfasda', 'sfsadsadsad', 'fajarnovianto@usa.com', '12321321321', 'fasdassadsa', 'dsdadsadsadsa', NULL, '2023-07-22 15:53:13', '2023-07-22 15:53:13');
INSERT INTO `contact_us` VALUES (109, 'feafsdas', 'dsadsads', 'fajarnovianto@usa.com', '12321321', 'fadsadsa', 'dsadsa', NULL, '2023-07-22 15:57:53', '2023-07-22 15:57:53');
INSERT INTO `contact_us` VALUES (110, 'test', 'testt11', 'fajarnovianto.fnfn@gmail.com', '12312321321', 'fasdsa', 'sdadsa', NULL, '2023-07-23 13:12:43', '2023-07-23 13:12:43');
INSERT INTO `contact_us` VALUES (111, 'gita', 'asdfsadf', 'sansicodigitalmarketing@gmail.com', '1234567', 'sdfgh', 'szfdxgh', NULL, '2023-07-24 03:05:01', '2023-07-24 03:05:01');
INSERT INTO `contact_us` VALUES (112, 'gita', 'asdf', 'sansicodigitalmarketing@gmail.com', '123456', 'asdfg', 'Szfdxgf', NULL, '2023-07-25 09:05:46', '2023-07-25 09:05:46');
INSERT INTO `contact_us` VALUES (113, 'gita', 'dfghj', 'sansicodigitalmarketing@gmail.com', '2345678', 'dfghjk', 'fghj', NULL, '2023-07-25 09:06:14', '2023-07-25 09:06:14');
INSERT INTO `contact_us` VALUES (114, 'NF', 'Computer', 'fajarnovianto@usa.com', '1238128391', 'fasdsajl', 'fjasdasa', NULL, '2023-07-25 09:13:38', '2023-07-25 09:13:38');
INSERT INTO `contact_us` VALUES (115, 'gita', 'asfd', 'sansicodigitalmarketing@gmail.com', '12345', 'asdf', 'ASdfg', NULL, '2023-07-25 10:06:29', '2023-07-25 10:06:29');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for home
-- ----------------------------
DROP TABLE IF EXISTS `home`;
CREATE TABLE `home`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `text1header` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `text2header` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `text3header` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imageheader` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `titlesection2` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imgsec2_1` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imgsec2_2` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imgsec2_3` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imgsec2_4` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `textsec2_1` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `textsec2_2` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `textsec2_3` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `textsec2_4` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `titlesec3` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `textsec3` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `imgsec3` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `titlesec5` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `descsec5` longtext CHARACTER SET utf8mb4 NULL,
  `imgsec5` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of home
-- ----------------------------
INSERT INTO `home` VALUES (1, 'We deliver', 'international standard F&B', 'and Aseptic packaging solution for Indonesia', 'https://nexpac.id/assets/images/home/background.jpg', 'WHY CHOOSE NEXPAC', 'https://nexpac.id/assets/images/home/1.png', 'https://nexpac.id/assets/images/home/2.png', 'https://nexpac.id/assets/images/home/3.png', 'https://nexpac.id/assets/images/home/4.png', '<p>Pioneer of Aseptic Packaging<br />\r\nManufacturer in Indonesia</p>', '<p>International<br />\r\nFood Safety Standard</p>', 'More than 30 years in<br />Printing & Packaging Industry', 'One Step<br />Packaging Solution', 'CUSTOMER COMMITMENT', 'We commited to produce food safe<br />\r\npackaging to serve our customer and<br />protect our earth with environtmental<br />safe packaging design and material.', 'https://nexpac.id/assets/images/home/Customer Commitment.jpg', 'CERTIFICATIONS', 'We are a fully certified company', 'https://nexpac.id/assets/images/home/2024-03-22-09-03-22.png', '2024-06-17 10:48:19', '2024-06-18 01:00:34');

-- ----------------------------
-- Table structure for instagram_contents
-- ----------------------------
DROP TABLE IF EXISTS `instagram_contents`;
CREATE TABLE `instagram_contents`  (
  `instagram_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `instagram_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`instagram_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of instagram_contents
-- ----------------------------
INSERT INTO `instagram_contents` VALUES (1, 'Instagram 0', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (2, 'Instagram 1', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (3, 'Instagram 2', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (4, 'Instagram 3', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (5, 'Instagram 4', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (6, 'Instagram 5', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (7, 'Instagram 6', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);
INSERT INTO `instagram_contents` VALUES (8, 'Instagram 7', 'https://www.insragram.com/nexpac.id', 'instagram.png', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for manufacturing
-- ----------------------------
DROP TABLE IF EXISTS `manufacturing`;
CREATE TABLE `manufacturing`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `header_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `title_heading` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `desc_heading` longtext CHARACTER SET utf8mb4 NULL,
  `section1_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section1_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section1_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section2_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section2_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section2_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section3_title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `section3_desc` longtext CHARACTER SET utf8mb4 NULL,
  `section3_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manufacturing
-- ----------------------------
INSERT INTO `manufacturing` VALUES (1, 'https://nexpac.id/assets/images/manufacturing/Banner.png', '<p>MANUFACTURING</p>', '<p>We commited to produce food safe packaging<br />\r\nto serve our customer and protect our earth with<br />\r\nenvirontmental safe packaging design and material.</p>', '<p>INTERNATIONAL<br />\r\nFOOD SAFETY STANDARD</p>', '<p>More than 30 years experience in the packaging industry.<br />\r\nNexpac machine uses aseptic standard for packaging<br />\r\nlamination procedurdees that follow ISO 9001:2015<br />\r\nand FSSC 22000.</p>', 'https://nexpac.id/assets/images/manufacturing/Fast Lead Time.png', 'TRACEABLE', 'We care about food safety and our customer<br />health. Nexpac committed to use food-grade<br />certifications material for our productions<br /><br />We care about our environtment. Nexpac<br />is FSC certified - ensures that products come<br />from responsibly mana', 'https://nexpac.id/assets/images/manufacturing/traceable.png', '<p>FAST LEAD TIME</p>', '<p>Work closely with our customer from development<br />\r\nuntil process production. Nexpac offer customized design services and our reliable delivery fleet is always<br />\r\nready to deliver our products to your doorstep.</p>', 'https://nexpac.id/assets/images/manufacturing/International food safety standard.png', '2024-06-17 11:45:15', '2024-06-18 01:51:04');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_reset_tokens_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2023_03_18_131557_create_products_table', 1);
INSERT INTO `migrations` VALUES (6, '2023_03_18_161822_create_certifications_table', 2);
INSERT INTO `migrations` VALUES (7, '2023_03_18_164045_create_instagram_contents_table', 3);
INSERT INTO `migrations` VALUES (8, '2023_03_18_235223_create_contact_us_table', 4);
INSERT INTO `migrations` VALUES (9, '2023_03_19_085933_create_applications_table', 5);
INSERT INTO `migrations` VALUES (10, '2023_03_22_075135_create_admins_table', 6);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `feature_image` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `news_created` datetime NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 NULL,
  `feature_post` tinyint NULL DEFAULT NULL,
  `read_post` bigint NULL DEFAULT 0,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (1, 'What is Lorem Ipsum?', 'https://nexpac.id/assets/images/manufacturing/Banner.png', 'Admin', '2024-06-17 16:14:48', 'Lorem Ipsu', 0, 0, '2024-06-17 16:15:06', '2024-06-17 16:32:54');
INSERT INTO `news` VALUES (2, 'Why do we use it?', 'https://nexpac.id/assets/images/manufacturing/Banner.png', 'Admin', '2024-06-17 16:31:04', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 0, 1, '2024-06-17 16:30:59', '2024-06-18 04:35:38');
INSERT INTO `news` VALUES (3, 'Where does it come from?', 'https://nexpac.id/assets/images/manufacturing/Banner.png', 'Admin', '2024-06-17 17:01:06', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 0, 0, '2024-06-17 17:01:02', '2024-06-17 17:01:17');
INSERT INTO `news` VALUES (8, 'helo', 'http://localhost:8000/assets/images/news/1718685851-4.jpg', 'admin', '2024-06-18 11:44:11', '<p>hello world</p>', NULL, 0, '2024-06-18 11:44:11', '2024-06-18 11:44:11');

-- ----------------------------
-- Table structure for news_cover
-- ----------------------------
DROP TABLE IF EXISTS `news_cover`;
CREATE TABLE `news_cover`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `header_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `title_heading` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news_cover
-- ----------------------------
INSERT INTO `news_cover` VALUES (1, 'https://nexpac.id/assets/images/manufacturing/Banner.png', '<p>NEWS</p>', '2024-06-17 16:00:46', '2024-06-18 02:04:27');

-- ----------------------------
-- Table structure for password_reset_tokens
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_tokens`;
CREATE TABLE `password_reset_tokens`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_reset_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for product_cover
-- ----------------------------
DROP TABLE IF EXISTS `product_cover`;
CREATE TABLE `product_cover`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `header_img` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `title_heading` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `sub1` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `sub2` varchar(255) CHARACTER SET utf8mb4 NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_cover
-- ----------------------------
INSERT INTO `product_cover` VALUES (1, 'https://nexpac.id/assets/images/products/background.jpg', 'FOOD SAFE EVERYWERE', 'Splyt-pak<sup>TM</sup> Aseptic Packaging', 'Food & Beverage Paper Packaging', '2024-06-17 15:51:34', '2024-06-18 06:59:52');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `product_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_about` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, '06oz (67ᴓ x 56mm), 08oz (80ᴓ x 92mm), 09oz (72ᴓ x 85mm), 12oz (87ᴓ x 105mm), 16oz (87ᴓ x 125mm), 22oz (87ᴓ x 160mm)', 'Paper Cup', 'https://nexpac.id/assets/images/products/paper packaging - Paper Cup.jpg', 'Food & Beverage', '<ul>\r\n<li>LDPE</li>\r\n<li>Hot/Cold</li>\r\n<li>Waterbased</li>\r\n<li>Coated outside or inside</li>\r\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (2, '10oz (90ᴓ x 76mm),12oz (107ᴓ x 65mm),17oz (110ᴓ x 78mm),22oz (126ᴓ x 68mm),24oz (140ᴓ x 70mm),27oz (130ᴓ x 87mm),33oz (147ᴓ x 93mm)', 'Paper Bowl', 'https://nexpac.id/assets/images/products/paper packaging - paperbowl.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (3, '65 ml', 'Splyt-pak 60 ml', 'https://nexpac.id/assets/images/products/60ml.jpg', 'Splyt-pak', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, NULL);
INSERT INTO `products` VALUES (4, 'S (100 x 100 x 50mm),S (123 x 70 x 50mm),M (160 x 90 x 50mm),L (180 x 110 x 50mm)', 'Lunch Box', 'https://nexpac.id/assets/images/products/paper packaging - lunch box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (5, 'S (120 x 92 x 65mm),M (150 x 120 x 65mm),L (80 x 65 x 100mm)', 'Pail Box', 'https://nexpac.id/assets/images/products/paper packaging - pail box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (6, '100 ml', 'Splyt-pak 100 ml', 'https://nexpac.id/assets/images/products/100ml.jpg', 'Splyt-pak', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, NULL);
INSERT INTO `products` VALUES (7, 'S (120 x 120 x 67mm),M (140 x 120 x 67mm),L (160 x 120 x 67mm)', 'Snack Box', 'https://nexpac.id/assets/images/products/paper packaging - snack box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (8, 'M (180 x 180 x 70mm),L (200 x 200 x 70mm)', 'Rice Box', 'https://nexpac.id/assets/images/products/paper packaging - box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (9, '200 ml', 'Splyt-pak 250 ml', 'https://nexpac.id/assets/images/products/250ml.jpg', 'Splyt-pak', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, NULL);
INSERT INTO `products` VALUES (10, 'S (95 x 95 x 75mm,M (110 x 110 x 86mm),L (110 x 110 x 115mm)', 'Burger Box', 'https://nexpac.id/assets/images/products/paper packaging - burger box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (11, 'M (170 x 60 x 80mm),L (210 x 60 x 80mm)', 'Hotdog Box', 'https://nexpac.id/assets/images/products/paper packaging - hotdog box.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);
INSERT INTO `products` VALUES (12, '1 Liter', 'Splyt-pak 1 Liter', 'https://nexpac.id/assets/images/products/1L.jpg', 'Splyt-pak', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, NULL);
INSERT INTO `products` VALUES (13, 'S (160 x 90 x 40mm),M (180 x 105 x 40mm),L (184 x 114 x 37mm)', 'Tray Paper', 'https://nexpac.id/assets/images/products/paper packaging - tray paper.jpg', 'Food & Beverage', '<ul>\n<li>LDPE</li>\n<li>Hot/Cold</li>\n<li>Waterbased</li>\n<li>Coated outside or inside</li>\n</ul>', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
