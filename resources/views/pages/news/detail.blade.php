@extends('pages.theme.layout')
@section('tittle', 'News')
@section('content')

	<style type="text/css">
		.content-header{
			position: relative;
			width: 100%;
			background: url("{{ $cover->header_img }}");
			background-position: top;
			background-repeat: no-repeat;
		}
		.top-manufact{
			bottom: 0px;
          	font-weight: normal;
			padding-top: 0.2rem;
		}

		@media screen and (max-width: 464px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 0 0 0;
			}
			h3.tittle-home{
				font-size: 12px;
                line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 50%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 10px;
			}
		}
		@media screen and (max-width: 767px) and (min-width: 465px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 10px 0 10px;
				line-height: 18px;
			}
			h3.tittle-home{
				font-size: 16px;
              	line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 30%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (max-width: 991px) and (min-width: 768px){
			.content-header{
				position: relative;
				height: calc(80vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 22px;
              	line-height: 1.70;
			}
			.manufacturing span{
				font-size: 13px;
				line-height: 18px;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 80%;
           		margin-left: 25px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (min-width: 992px){
			.content-header{
				position: relative;
				height: calc(85vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 25px;
              	line-height: 1.70;
			}
			.manufacturing-content img{
				width: 80%;
			}
			.manufacturing-content h5{
				font-size: 18px;
              	margin-left: 25px;
			}

			.manufacturing-content{
				margin: 70px 50px 0 50px;
			}
            .top-manufact h1{
				font-size: 3rem;
              	font-weight: normal;
			}
		}
        .news{
            padding: 30px 50px;
        }
	</style>

	<section class="content-header d-flex align-items-center justify-content-center overflow-hidden">
		<div class="text-white bg-blue position-absolute text-center w-100 px-md-5 px-3 top-manufact">
			<h1 style="font-weight: normal;">
				<?= ''.$cover->title_heading ?>
			</h1>
		</div>
	</section>
	<section class="news">
		<div class="container">
			<div class="row">
                <h4 style="color: #034e9e"><b>{{ $news->title }}</b></h4>
                <p>Posted by <b>{{ $news->author }}</b>, <i>{{ date("F d, Y | H:i", strtotime($news->news_created)) }}</i></p>
                <img src="{{ $news->feature_image }}" class="img-fluid" /><br>
                <div class="content"><?= ''.$news->content ?></div>
			</div>
		</div>
	</section>
	<section class="manufacturing-content"></section>
	<script type="text/javascript">
		document.getElementsByClassName('menu-top')[5].classList.add('active');
	</script>
@endsection
