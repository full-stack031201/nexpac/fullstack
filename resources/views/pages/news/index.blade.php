@extends('pages.theme.layout')
@section('tittle', 'News')
@section('content')

	<style type="text/css">
		.content-header{
			position: relative;
			width: 100%;
			background: url("{{ $cover->header_img }}");
			background-position: top;
			background-repeat: no-repeat;
		}
		.top-manufact{
			bottom: 0px;
          	font-weight: normal;
			padding-top: 0.2rem;
		}

		@media screen and (max-width: 464px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 0 0 0;
			}
			h3.tittle-home{
				font-size: 12px;
                line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 50%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 10px;
			}
		}
		@media screen and (max-width: 767px) and (min-width: 465px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 10px 0 10px;
				line-height: 18px;
			}
			h3.tittle-home{
				font-size: 16px;
              	line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 30%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (max-width: 991px) and (min-width: 768px){
			.content-header{
				position: relative;
				height: calc(80vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 22px;
              	line-height: 1.70;
			}
			.manufacturing span{
				font-size: 13px;
				line-height: 18px;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 80%;
           		margin-left: 25px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (min-width: 992px){
			.content-header{
				position: relative;
				height: calc(85vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 25px;
              	line-height: 1.70;
			}
			.manufacturing-content img{
				width: 80%;
			}
			.manufacturing-content h5{
				font-size: 18px;
              	margin-left: 25px;
			}

			.manufacturing-content{
				margin: 70px 50px 0 50px;
			}
            .top-manufact h1{
				font-size: 3rem;
              	font-weight: normal;
			}
		}
        .news{
            padding: 30px 50px;
        }
        a {
            color: black;
        }
        a:hover {
            color: blue;
        }
        .card:hover {
            border-color: blue;
        }
        .card-text {
            color: gray;
            font-size: .875em;
        }
        .news-img-wrapper {
            position: relative;
            width: 100%;
            padding-top: 80%;
            overflow: hidden;
        }

        .news-img {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
	</style>

	<section class="content-header d-flex align-items-center justify-content-center overflow-hidden">
		<div class="text-white bg-blue position-absolute text-center w-100 px-md-5 px-3 top-manufact">
			<h1 style="font-weight: normal;">
				<?= ''.$cover->title_heading ?>
			</h1>
		</div>
	</section>

	<section class="news">
		<div class="container">
            <div class="row">
                @foreach ( $news as $nw )
                    <div class="col-md-4">
                        <a style="text-decoration: none" href="{{ route('news.detail', [$nw->id, \Str::slug($nw->title, '-')]) }}">
                            <div class="card" style="margin-bottom: 20px; height: 97.5%">
                                <div class="news-img-wrapper">
                                    <img src="{{ $nw->feature_image }}" class="card-img-top news-img">
                                </div>
                                <div style="padding-left: 18px;"><small>{{ date('F d, Y | H:i', strtotime($nw->news_created)) }}</small></div>
                                <div class="card-body">
                                    <h5 class="card-title">{{ \Str::limit($nw->title, 50, '...') }}</h5>
                                    <p class="card-text"><?= ''.\Str::limit(strip_tags($nw->content), 150, '...') ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="d-flex justify-content-center">
                {{-- mengarah ke pagination tailwind --}}
                {{ $news->links() }}
            </div>
		</div>
	</section>
@endsection
