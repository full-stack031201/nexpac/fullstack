{{--
if (isset($_POST['login'])){
	$user=strip_word_html(strip_word_html($_POST['username']));
	$pass=md5(md5(strip_word_html($_POST['password'])));
	$sql=mysqli_query($con, "SELECT * FROM admins WHERE username='$user' AND password='$pass' AND status='1'");
	$row=mysqli_fetch_array($sql);
	if (mysqli_num_rows($sql) > 0){
		$_SESSION['user_nexpac']=$user;
		$_SESSION['usertype_nexpac']=$row['usertype'];
		$_SESSION['id_login_nexpac']=$row['id_login'];
		$_SESSION['timestamp_nexpac']=time();
		if (isset($next))
			_direct($next);
		else
			_direct("index.php");
	} else {
		$pesan="Login gagal. Coba lagi.";
		$warna="red";
	}
} --}}

{{-- if(isset($_SESSION['user_nexpac'])) {
	if (isset($next))
		_direct($next);
	else
		_direct("index.php");
} --}}

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets-panel/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="assets-panel/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets-panel/assets/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <!--div class="card-header text-center">
      <h1><b>nexpac</b></h1>
    </div-->
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      @if (Session::has('error'))

        <div class="text-center mb-2"><span class="badge bg-red">{{ Session::get('error') }}</span></div>

      @endif

      <form action="" method="post">
        @csrf
        <input type="hidden" name="login" value="true">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="assets-panel/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets-panel/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="assets-panel/assets/js/adminlte.min.js"></script>
</body>
</html>
