@extends('pages.theme.layout')
@section('tittle', 'Manufacturing')
@section('content')

	<style type="text/css">
		.content-header{
			position: relative;
			width: 100%;
			background: url("{{ $manufactur->header_img }}");
			background-position: top;
			background-repeat: no-repeat;
		}
		.top-manufact{
			bottom: 0px;
          	font-weight: normal;
			padding-top: 0.2rem;
		}

		@media screen and (max-width: 464px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 0 0 0;
			}
			h3.tittle-home{
				font-size: 12px;
                line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 50%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 10px;
			}
		}
		@media screen and (max-width: 767px) and (min-width: 465px){
			.content-header{
				position: relative;
				height: calc(60vh - 10vh);
			}
			.manufacturing{
				margin: 50px 10px 0 10px;
				line-height: 18px;
			}
			h3.tittle-home{
				font-size: 16px;
              	line-height: 1.70;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 30%;
				margin-bottom: 10px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (max-width: 991px) and (min-width: 768px){
			.content-header{
				position: relative;
				height: calc(80vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 22px;
              	line-height: 1.70;
			}
			.manufacturing span{
				font-size: 13px;
				line-height: 18px;
			}
			.manufacturing-content{
				margin: 50px 10px 0 10px;
			}
			.manufacturing-content img{
				width: 80%;
           		margin-left: 25px;
			}
			.manufacturing-content h5{
				font-size: 12px;
			}
		}
		@media screen and (min-width: 992px){
			.content-header{
				position: relative;
				height: calc(85vh - 10vh);
				background-size: 100% auto;
			}
			.manufacturing{
				margin: 50px 50px 0 50px;
			}
			h3.tittle-home{
				font-size: 25px;
              	line-height: 1.70;
			}
			.manufacturing-content img{
				width: 80%;
			}
			.manufacturing-content h5{
				font-size: 18px;
              	margin-left: 25px;
			}

			.manufacturing-content{
				margin: 70px 50px 0 50px;
			}
            .top-manufact h1{
				font-size: 3rem;
              	font-weight: normal;
			}
		}
	</style>

	<section class="content-header d-flex align-items-center justify-content-center overflow-hidden">
		<div class="text-white bg-blue position-absolute text-center w-100 px-md-5 px-3 top-manufact">
			<h1 style="font-weight: normal;">
				<?= ''.$manufactur->title_heading ?>
			</h1>
		</div>
	</section>
	<section class="manufacturing">
		<div class="container">
			<div class="text-center text-tersier-nexpac mx-md-5 text-grey" data-aos="fade-down">
				<h3 class="tittle-home" style="font-weight: normal; line-height: 1.5; color:#6d6e71 !important;" >
					<?= $manufactur->desc_heading; ?>
				</h3>
			</div>
		</div>
	</section>
	<section class="">
		<div class="container">
			<div class="mx-lg-5 mx-md-3">
				<div class="d-md-flex align-items-center manufacturing-content" data-aos="fade-down" style="margin-top: 150px;">
					<div class="col-md-3">
						<img src="<?= $manufactur->section1_img; ?>">
					</div>
					<div>
						<div class="ms-2 text-gray manufacturing-documentation">
							<h5 class="text-blue mb-2" style="line-height: 1.5; font-size: 40px !important; margin-left: 0px!important;"><?= $manufactur->section1_title; ?></h5>
                            <div class="border-1 border-blue mt-1 mb-2 aplication-break"></div>
							<h5 class="text-gray" style="margin: 0; font-weight: 400; line-height: 1.6;"><?= $manufactur->section1_desc; ?></h5>
							</h5>
						</div>
					</div>
				</div>
				<div class="d-md-flex align-items-center manufacturing-content text-end" data-aos="fade-down" style="margin-top: 200px;">
					<div class="col-md-3">
						<img src="<?= $manufactur->section2_img; ?>" class="d-md-none">
					</div>
					<div class="col-md-6 text-end">
						<div class="me-2 text-tersier-nexpac manufacturing-documentation">
							<h5 class="text-blue mb-2" style="font-size: 40px !important;"><?= $manufactur->section2_title; ?></h5>
                          <div class="border-1 border-blue mt-1 mb-2 aplication-break"></div>
							<h5 class="text-gray" style="margin: 0; font-weight: 400;line-height: 1.6;"><?= $manufactur->section2_desc; ?></h5>
							</h5>
						</div>
					</div>
					<div class="col-md-3 d-md-block d-none">
						<img src="<?= $manufactur->section2_img; ?>">
					</div>
				</div>
				<div class="d-md-flex align-items-center manufacturing-content" data-aos="fade-down" style="margin-top: 200px;">
					<div class="col-md-3">
						<img src="<?= $manufactur->section3_img; ?>">
					</div>
					<div class="col-md-6">
						<div class="ms-2 text-tersier-nexpac manufacturing-documentation">
							<h5 class="text-blue mb-2" style="font-size: 40px !important; margin-left: 0px!important;"><?= $manufactur->section3_title; ?></h5>
                          <div class="border-1 border-blue mt-1 mb-2 aplication-break"></div>
							<h5 class="text-gray" style="margin: 0; font-weight: 400;line-height: 1.6;"><?= $manufactur->section3_desc; ?></h5>
							</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="manufacturing-content"></section>
	<script type="text/javascript">
		document.getElementsByClassName('menu-top')[2].classList.add('active');
	</script>
@endsection
