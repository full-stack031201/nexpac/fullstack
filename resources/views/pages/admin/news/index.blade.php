@extends('pages.admin.theme.layout')
@section('title', 'News - Add Post')
@section('content')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">View Post</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">News</li>
              <li class="breadcrumb-item active">View Post</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <?php
          if (isset($pesan)){
            $warna=($warna=="green" ? "success" : "failed");
            echo '<div style="margin: 0 0 10px 10px;"><span class="badge ' .$warna. '">' .$pesan. '</span></div>';
          }
          ?>
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  View Post
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <table id="table2" class="table table-bordered table-striped">
          				<thead>
          					<tr>
          						<th>#</th>
          						<th>Title</th>
          						<th>Author</th>
          						<th>Date</th>
          						<th>Reads</th>
          						<th></th>
          					</tr>
          				</thead>
          				<tbody>
                            @foreach ($data['news'] as $index => $nw )
                                <tr>
                                    <td align="center"><a href="{{ route('news.edit', $nw->id) }}"><div style="min-width:70px">{{ $index + 1 }}</div></a></td>
                                    <td><a href="{{ route('news.edit', $nw->id) }}"><div style="min-width:70px">{{ $nw->title }}</div></a></td>
                                    <td><a href="{{ route('news.edit', $nw->id) }}"><div style="min-width:70px">{{ $nw->author }}</div></a></td>
                                    <td><a href="{{ route('news.edit', $nw->id) }}"><div style="min-width:70px">{{ date('F d, Y | H:i', strtotime($nw->news_created)) }}</div></a></td>
                                    <td align="center"><a href="{{ route('news.edit', $nw->id) }}"><div style="min-width:70px">{{ $nw->read_post }}</div></a></td>
                                    <td><a href="javascript:;" onclick="get_delete({{ $nw->id }})" class="btn btn-primary btn-xs" title="DELETE"><i class="fa fa-trash"></i> Hapus</a></td>
                                </tr>
                            @endforeach
          				</tbody>
          			</table>
              </div><!-- /.card-body -->
            </div>


            <!-- /.card -->
          </section>


          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Delete this Content ?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure wanna delete this content ?</p>
          <form method="post">
            <input type="hidden" name="del_content" value="true" />
            <input type="hidden" name="id" id="id" value="" />
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
          <input type="button" id="submit-delete" class="btn btn-danger" value="Submit">
        </form>
        </div>
      </div>
    </div>
  </div>

<script>
    let token   = $("meta[name='csrf-token']").attr("content");
  function get_delete(id){
    $('#id').val(id);
    $('#myModal').modal('show');
  }

  $('#submit-delete').click(function(){
        $('#submit-delete').val('loading ...')
        let id = $('#myModal').find('input[name=id]').val()
        $.ajax({
            url: '/admin/news/'+id,
            type: 'DELETE',
            cache: false,
            data: {
                "_token": token
            },
            success:function(response){
                window.location.reload()
                $('#submit-delete').val('Submit')
            },
            error: function(error){
                $('#submit-delete').val('Submit')
                console.error(error)
            }
        })
    })
</script>

@endsection
