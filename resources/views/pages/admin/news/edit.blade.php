@extends('pages.admin.theme.layout')
@section('title', 'News - Edit Post')
@section('content')

<style>
    .ck-editor__editable_inline {
            min-height: 700px; /* Sesuaikan nilai ini untuk mengatur tinggi editor */
        }
</style>


{{-- <script src="{{ asset('assets-panel/ckeditor2/ckeditor.js') }}"></script> --}}
{{-- <script src="https://cdn.ckeditor.com/ckeditor5/39.0.0/classic/ckeditor.js"></script> --}}
<script src="{{ asset('assets/tinymce/js/tinymce/tinymce.min.js') }}"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">View Post</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">News</li>
              <li class="breadcrumb-item active">Add Post</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  View Post
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('news.update', $data['news']->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
          				<div class="form-group">
          					<label>Title :</label>
          					<input class="form-control" type="text" name="title" maxlength="150" value="{{ $data['news']->title }}" required>
          				</div>
          				<div class="form-group">
                      <label>Content :</label><br />
                      <small>
                        * cara untuk mengupload gambar :<br />
                        1. Jika gambar ada di penyimpanan lokal, silahkan copy gambar lalu tempelkan gambar di tempat yang sesuai.<br />
                        2. Jika gambar berada di link tertentu, silahkan klik menu insert -> image lalu isi field source dengan url gambar anda<br />
                    </small><br />
                      <textarea name="content" id="content" class="form-control">{{ $data['news']->content }}</textarea>
                  </div>
          				<div class="form-group">
          					<label>Featured Image :</label><br>
          					<img id="pic1" class="img-responsive" width="480" src="{{ $data['news']->feature_image }}"><br /><br />
          					<input type="file" name="feature_image" id="img1">
          				</div>
          				<div class="clearfix"></div>
          				<input type="submit" class="btn btn-warning" value="Simpan">
          			</form>
              </div><!-- /.card-body -->
            </div>


            <!-- /.card -->
          </section>


          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<script>
    tinymce.init({
        selector: 'textarea#content',
        plugins: 'image',
        toolbar: 'undo redo | blocks | bold italic | alignleft aligncentre alignright alignjustify | indent outdent | bullist numlist',
    });
  function refresh_pic(){
    var str="../assets/images/news/";
    var img=$('#img1').val();
    $("#pic1").attr("src", str + img);
    $('#img1').val(str + img);
  }
    $('#img1').change(function(event){
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                const previewImage = document.getElementById('pic1');
                previewImage.src = e.target.result;
            }
            reader.readAsDataURL(file);
        } else {
            const previewImage = document.getElementById('pic1');
            previewImage.src ="{{ $data['news']->feature_image }}";
        }
    })

  function get_delete(id){
    $('#id').val(id);
    $('#myModal').modal('show');
  }

  $(document).ready(function(){

  });

</script>

@endsection
