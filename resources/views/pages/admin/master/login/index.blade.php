@extends('pages.admin.theme.layout')
@section('title', 'Master - Login')
@section('content')

<style>
.error {
  color: red;
  -webkit-text-stroke: 5px white;
  paint-order: stroke fill;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Master Login</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Master Login</li>
              <li class="breadcrumb-item active">View</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Master Login
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary" style="float: right"><i class="fa fa-plus"></i> Add User</a>
                <div class="clearfix"></div><br>
                <table class="table table-bordered table-striped">
                  <thead>
                    <th>No</th>
                    <th>Username</th>
                    <th>Status</th>
                    <th></th>
                  </thead>
                  <tbody>
                    @foreach ($data['admin'] as $index => $adm)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $adm->username }}</td>
                            <td align="center">@if($adm->status == 0)<i class="fa fa-times"></i> @else <i class="fa fa-check"></i> @endif</td>
                            <td align="center">
                                <a onClick="get_edit('{{ $adm->id }}')" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
                                <a onClick="get_delete('{{ $adm->id }}')" class="btn btn-xs btn-danger" alt="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- /.Left col -->

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Delete this User ?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure wanna delete this user ?</p>
          <form>
            <input type="hidden" name="del_content" id="content" value="" />
            <input type="hidden" name="id" id="id" value="" />
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
          <input type="button" class="btn btn-danger" value="Submit" id="submit-delete">
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal2">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Add User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form1" action="" method="post">
            @csrf
            <input type="hidden" name="add_login_post" value="true">
            <h5>Username :</h5>
            <input type="text" class="form-control" name="username" value="" autocomplete="off" required />
            <h5>Password :</h5>
            <input type="password" class="form-control" name="pass" value="" autocomplete="off" required />
            <h5>Status :</h5>
            <select class="form-control" name="status" value="" required />
              <option value="" selected disabled>Select Status</option>
              <option value="1">Enabled</option>
              <option value="0">Disabled</option>
            </select>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal3">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Edit User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form2" method="post" action="">
            @csrf
            <input type="hidden" name="edit_login_post" value="true">
            <div id="edit_user">
                <input type="hidden" id="id_log" name="id_login">
				 <h5>Username</h5>
				 <input class="form-control" type="text" name="user" maxlength="255" disabled>
				 <h5>Password</h5>
				 <a class="btn btn-success" id="chg_pwd"><i class="fa fa-lock fa-md"></i> Change Password</a>
				 <h5>Status</h5>
				 <select class="form-control" id="status" name="status" required>
					<option value="" selected disabled>Select Status</option>
					<option value="1">Enabled</option>
					<option value="0">Disabled</option>
				 </select>
            </div>
   			<div class="clearfix"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal4">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Edit Password</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form3" action="" method="post" onsubmit="return post_this();">
            @csrf
            <input type="hidden" name="edit_password_post" value="true">
            <input type="text" id="id_login" name="id_login" value="">
   				  <h5>Password</h5>
   				  <input class="form-control" type="password" id="pass_1" name="pass_1" maxlength="255" required>
   				  <h5>Confirm Password</h5>
   				  <input class="form-control" type="password" id="pass_2" name="pass_2" maxlength="255" required>
   				 <div class="clearfix"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
        </form>
        </div>
      </div>
    </div>
  </div>

  <script>
    let token   = $("meta[name='csrf-token']").attr("content");

    function get_edit(id){
        $.ajax({
            url: `/master/login/${id}`,
            method: 'GET',
            success: function(response){
                $('#edit_user').find('input[name=id_login]').val(response.data.id);
                $('#edit_user').find('input[name=user]').val(response.data.username);
                $('#edit_user').find('select[name=status]').val(response.data.status);
                $('#myModal3').find('form').attr('action', '/master/login/'+response.data.id)
                $('#form2').validate();
    	        $('#myModal3').modal('show');
            },
            error: function(error){
                console.error(error)
            }
        })
    }

    function get_delete(id){
        $('#id').val(id);
        $('#myModal').modal('show');
    }

    $('#submit-delete').click(function(){
        $('#submit-delete').val('loading ...')
        let id = $('#myModal').find('input[name=id]').val()
        $.ajax({
            url: '/master/login/'+id,
            type: 'DELETE',
            cache: false,
            data: {
                "_token": token
            },
            success:function(response){
                window.location.reload()
                $('#submit-delete').val('Submit')
            },
            error: function(error){
                $('#submit-delete').val('Submit')
                console.error(error)
            }
        })
    })

    $('#chg_pwd').click(function(){
        let id = $('#edit_user').find('input[name=id_login]').val();
        $('#myModal4').find('input[name=id_login]').val(id);
        $('#myModal4').find('form').attr('action', '/master/login/'+id)
        $('#myModal4').modal('show')
    })
    function post_this(){
    	if ($('#pass_1').val()==$('#pass_2').val()) {
    		return true;
    	} else {
    		alert('Passwords do NOT match');
    		return false;
    	}
    }

    $(document).ready(function(){
      $('#form1').validate();
      $('#form3').validate();
    });
  </script>



@endsection
