@extends('pages.admin.theme.layout')
@section('title', 'Master - Application')
@section('content')

    <style>
    .inputfile{
      padding-bottom: 15px;
    }
    </style>

    <script src="{{ asset('assets-panel/ckeditor2/ckeditor.js') }}"></script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Master Application</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item active">Application</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                @if(Session::has('success'))
                    <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
                @endif
                @if(Session::has('error'))
                    <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
                @endif
              <!-- Left col -->
              <section class="col-lg-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card collapsed-card">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fas fa-images mr-1"></i>
                      Header Image
                    </h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div><!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-4">
                        <img src="{{ $data['category'][0]->header_img }}" width="300" class="img-thumbnail" />
                      </div>
                      <div class="col-md-8">
                        <div class="inputfile">
                          <h5>Header Image Aseptic Packaging (*.png)</h5>
                          <form action="" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="update_imageheader_aseptic_post" value="true">
                            <input type="file" name="aseptic_photo" required accept=".png" /><br><br>
                            <input type="submit" class="btn btn-primary" value="Upload">
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div><br><br>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="inputfile">
                          <h5>Header Image Paper Packaging (*.png)</h5>
                          <form action="" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="update_imageheader_paper_post" value="true">
                            <input type="file" name="paper_photo" required accept=".png" /><br><br>
                            <input type="submit" class="btn btn-primary" value="Upload">
                          </form>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <img src="{{ $data['category'][1]->header_img }}" width="300" class="img-thumbnail" />
                      </div>

                    </div>
                  </div><!-- /.card-body -->
                </div>

                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">
                        <i class="fas fa-images mr-1"></i>
                        Master Application
                      </h3>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-plus"></i>
                        </button>
                      </div>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                      <a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary" style="float: right"><i class="fa fa-plus"></i> Add App</a>
                      <div class="clearfix"></div><br>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <th>No</th>
                          <th>Name</th>
                          <th>Spec</th>
                          <th>About</th>
                          <th>Category</th>
                          <th></th>
                        </thead>
                        <tbody>
                            @foreach ($data['application'] as $index => $app)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td><img src="{{ $app->application_picture }}" width="50"><br><?= ''.$app->application_name ?></td>
                              <td><?= $app->application_spec ?></td>
                              <td><?= ''.$app->application_about ?></td>
                              <td><?= \App\Models\ApplicationCategories::find($app->application_category)->title_heading ?></td>
                              <td align="center">
                                <a onClick="get_edit({{ $app->application_id }})" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
                                <a onClick="get_delete({{ $app->application_id }})" class="btn btn-xs btn-danger" alt="Delete"><i class="fa fa-trash"></i></a>
                              </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div><!-- /.card-body -->
                  </div>

                {{-- <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fas fa-images mr-1"></i>
                      Master Application
                    </h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div><!-- /.card-header -->
                  <div class="card-body">
                    <a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary" style="float: right"><i class="fa fa-plus"></i> Add App</a>
                    <div class="clearfix"></div><br>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <th>No</th>
                        <th>Name</th>
                        <th>Spec</th>
                        <th>About</th>
                        <th>Category</th>
                        <th></th>
                      </thead>
                      <tbody>
                        <?php
                        $sql=mysqli_query($con, "SELECT * FROM applications");
                        $x=0;
                        while ($row=mysqli_fetch_array($sql)){
                          $x++;
                          echo '<tr>
                            <td>' .$x. '</td>
                            <td><img src="../' .$row['application_picture']. '" width="50"><br>' .$row['application_name']. '</td>
                            <td>' .$row['application_spec']. '</td>
                            <td>' .$row['application_about']. '</td>
                            <td>' .$row['application_category']. '</td>
                            <td align="center">
                              <a onClick="get_edit(' .$row['application_id']. ')" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
                              <a onClick="get_delete(' .$row['application_id']. ')" class="btn btn-xs btn-danger" alt="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>';
                        }
                        ?>
                      </tbody>
                    </table>
                  </div><!-- /.card-body -->
                </div> --}}
                <!-- /.card -->
              </section>


              <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>

      <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content bg-default">
            <div class="modal-header">
              <h4 class="modal-title">Delete this Content ?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure wanna delete this content ?</p>
              <form method="post">
                <input type="hidden" name="del_content" value="true" />
                <input type="hidden" name="id" id="id" value="" />
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
              <input type="button" id="submit-delete" class="btn btn-danger" value="Submit">
            </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="myModal2">
        <div class="modal-dialog">
          <div class="modal-content bg-default">
            <div class="modal-header">
              <h4 class="modal-title">Add Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="?action=add_data" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="add_app_post" value="true">
                <h5>Name :</h5>
                <input type="text" class="form-control" name="name" autocomplete="off" value="" required />
                <h5>Spec :</h5>
                <input type="text" class="form-control" name="spec" autocomplete="off" value="" />
                <h5>About :</h5>
                <textarea class="form-control" id="about" name="about" autocomplete="off" value=""></textarea>
                <h5>Category :</h5>
                <select class="form-control" name="category" required>
                  <option value="" selected disabled>Select Category</option>
                  @foreach ($data['category'] as $index => $cat)
                    <option value="{{ $cat->id }}">{{ $cat->title_heading }}</option>
                  @endforeach
                </select>
                <h5>Image :</h5>
                <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" required /><br>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
              <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
            </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="myModal3">
        <div class="modal-dialog">
          <div class="modal-content bg-default">
            <div class="modal-header">
              <h4 class="modal-title">Edit Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="?action=edit_data" method="post"  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="edit_app_post" value="true">
                <div id="edit_app">
                    <input type="hidden" name="id" value="">
                    <h5>Name :</h5>
                    <input type="text" class="form-control" name="name" autocomplete="off" value="" required />
                    <h5>Spec :</h5>
                    <input type="text" class="form-control" name="spec" autocomplete="off" value="" />
                    <h5>About :</h5>
                    <textarea class="form-control" id="about_2" name="about"></textarea>
                    <h5>Category :</h5>
                    <select class="form-control" id="cat" name="category" required>
                        <option value="" selected disabled>Select Category</option>
                        @foreach ($data['category'] as $index => $cat)
                            <option value="{{ $cat->id }}">{{ $cat->title_heading }}</option>
                        @endforeach
                    </select>
                    <input type="checkbox" id="update" name="update_image">
                    <label for="update">Update Image</label>
                </div>
                <div id="div_update" class="d-none">
                    <h5>Image :</h5>
                    <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
                </div>
               <div class="clearfix"></div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
              <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
            </form>
            </div>
          </div>
        </div>
      </div>

    <script>
        let token   = $("meta[name='csrf-token']").attr("content");

      var editor = CKEDITOR.replace('about');
      var editor = CKEDITOR.replace('about_2');

      function get_edit(id){
        $.ajax({
            url: `/master/application/${id}`,
            method: 'GET',
            success: function(response){
                $('#edit_app').find('input[name=id]').val(response.data.application_id);
                $('#edit_app').find('input[name=name]').val(response.data.application_name);
                $('#edit_app').find('input[name=spec]').val(response.data.application_spec);
                $('#edit_app').find('select[name=category]').val(response.data.application_category);
                CKEDITOR.instances['about_2'].setData(response.data.application_about);
                $('#myModal3').modal('show');
            },
            error: function(error){
                console.error(error)
            }
        })
      }

      function get_delete(id){
        $('#id').val(id);
        $('#myModal').modal('show');
      }

      $('#submit-delete').click(function(){
        $('#submit-delete').val('loading ...')
        let id = $('#myModal').find('input[name=id]').val()
        $.ajax({
            url: '/master/application/'+id,
            type: 'DELETE',
            cache: false,
            data: {
                "_token": token
            },
            success:function(response){
                window.location.reload()
                $('#submit-delete').val('Submit')
            },
            error: function(error){
                $('#submit-delete').val('Submit')
                console.error(error)
            }
        })
    })

      $('input#update').change(function(){
        if($(this).is(':checked')){
            $('#div_update').removeClass('d-none')
            $('#div_update').find('input[name=photo]').attr('required', true)
        } else {
            $('#div_update').addClass('d-none')
            $('#div_update').find('input[name=photo]').removeAttr('required')
        }
      })
    </script>

@endsection
