@extends('pages.admin.theme.layout')
@section('title', 'Master - Product')
@section('content')

<script src="{{ asset('assets-panel/ckeditor2/ckeditor.js') }}"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Master Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Master</li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Header Image
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $data['cover']->header_img }}" alt="" width="300" class="img-thumbnail">
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <input required type="hidden" name="update_imageheader_post" value="true">
                        <input type="file" name="header_img" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Master Product
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary" style="float: right"><i class="fa fa-plus"></i> Add Product</a>
                <div class="clearfix"></div><br>
                <table class="table table-bordered table-striped">
                  <thead>
                    <th>No</th>
                    <th>Name</th>
                    <th>Size</th>
                    <th>About</th>
                    <th>Category</th>
                    <th></th>
                  </thead>
                  <tbody>
                    @foreach ($data['product'] as $index => $product)
                        <tr>
                            <td>'{{ $index + 1 }}</td>
                            <td><img src="{{ $product->product_picture }}" width="50"><br>{{ $product->product_name }}</td>
                            <td>{{ $product->product_size }}</td>
                            <td><?= ''.$product->product_about ?></td>
                            <td>{{ $product->product_category }}</td>
                            <td align="center">
                            <a onClick="get_edit({{ $product->product_id }})" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
                            <a onClick="get_delete({{ $product->product_id }})" class="btn btn-xs btn-danger" alt="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>


          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Delete this Content ?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure wanna delete this content ?</p>
          <form method="post">
            <input type="hidden" name="del_content" value="true" />
            <input type="hidden" name="id" id="id" value="" />
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
          <input type="button" id="submit-delete" class="btn btn-danger" value="Submit">
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal2">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Add Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="?action=add_data" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="add_app_post" value="true">
            <h5>Name :</h5>
            <input type="text" class="form-control" name="name" autocomplete="off" value="" required />
            <h5>Size :</h5>
            <input type="text" class="form-control" name="size" autocomplete="off" value="" />
            <h5>About :</h5>
            <textarea type="text" class="form-control" id="about" name="about" autocomplete="off" value=""></textarea>
            <h5>Category :</h5>
            <select class="form-control" name="category" required>
              <option value="" selected disabled>Select Category</option>
              <option value="Food & Beverage">Food & Beverage</option>
              <option value="Splyt-pak">Splyt-pak</option>
            </select>
            <h5>Image :</h5>
            <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" required /><br>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
        </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal3">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Edit Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="?action=edit_data" method="post"  enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="edit_app_post" value="true">
            <div id="edit_app">
                <input type="hidden" name="id" value="">
				 <h5>Name :</h5>
				 <input type="text" class="form-control" name="name" autocomplete="off" value="" required />
				 <h5>Size :</h5>
				 <input type="text" class="form-control" name="size" autocomplete="off" value="" />
				 <h5>About :</h5>
				 <textarea class="form-control" id="about_2" name="about" autocomplete="off"></textarea>
				 <h5>Category :</h5>
				 <select class="form-control" id="cat" name="category" required>
					 <option value="" selected disabled>Select Category</option>
					 <option value="Food & Beverage">Food & Beverage</option>
					 <option value="Splyt-pak">Splyt-pak</option>
				 </select>
				 <input type="checkbox" id="update" name="update_image">
				 <label for="update">Update Image</label>
            </div>
            <div id="div_update" class="d-none">
   					 <h5>Image :</h5>
   					 <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
   				 </div>
           <div class="clearfix"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          <input type="submit" class="btn btn-warning" value="Submit" style="margin-top: 15px;" />
        </form>
        </div>
      </div>
    </div>
  </div>

<script>
    let token   = $("meta[name='csrf-token']").attr("content");
    var editor = CKEDITOR.replace('about');
    var editor = CKEDITOR.replace('about_2');

    function get_edit(id){
        $.ajax({
            url: `/master/product/${id}`,
            method: 'GET',
            success: function(response){
                $('#edit_app').find('input[name=id]').val(response.data.product_id);
                $('#edit_app').find('input[name=name]').val(response.data.product_name);
                $('#edit_app').find('input[name=size]').val(response.data.product_size);
                $('#edit_app').find('select[name=category]').val(response.data.product_category);
                CKEDITOR.instances['about_2'].setData(response.data.product_about);
                $('#myModal3').modal('show');
            },
            error: function(error){
                console.error(error)
            }
        })
    }

    function get_delete(id){
        $('#id').val(id);
        $('#myModal').modal('show');
    }
    $('#submit-delete').click(function(){
        $('#submit-delete').val('loading ...')
        let id = $('#myModal').find('input[name=id]').val()
        $.ajax({
            url: '/master/product/'+id,
            type: 'DELETE',
            cache: false,
            data: {
                "_token": token
            },
            success:function(response){
                window.location.reload()
                $('#submit-delete').val('Submit')
            },
            error: function(error){
                $('#submit-delete').val('Submit')
                console.error(error)
            }
        })
    })

    $('input#update').change(function(){
        if($(this).is(':checked')){
            $('#div_update').removeClass('d-none')
            $('#div_update').find('input[name=photo]').attr('required', true)
        } else {
            $('#div_update').addClass('d-none')
            $('#div_update').find('input[name=photo]').removeAttr('required')
        }
    })
</script>

@endsection
