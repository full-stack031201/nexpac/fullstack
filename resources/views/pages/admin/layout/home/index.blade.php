@extends('pages.admin.theme.layout')
@section('title', 'Layouts - Home')
@section('content')

<style>
    .inputfile{
      padding-bottom: 25px;
    }
</style>
<script src="{{ asset('assets-panel/ckeditor2/ckeditor.js') }}"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Layout Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Layout</li>
              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Header
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4"><img src="{{ $data['home']->imageheader }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="?action=update_imageheader_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_imageheader_post" value="true">
                        <input type="file" required name="photo" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h5>Header 1</h5>
                  </div>

                  <div class="col-md-12">
                    <form method="post" action="?action=update_header_post">
                        @csrf
                      <input type="hidden" name="update_header_post" value="true">
                      <input type="text" class="form-control" name="header1" autocomplete="off" value="{{ $data['home']->text1header }}" />
                      <h5>Header 2</h5>
                      <input type="text" class="form-control" name="header2" autocomplete="off" value="{{ $data['home']->text2header }}" />
                      <h5>Header 3</h5>
                      <input type="text" class="form-control" name="header3" autocomplete="off" value="{{ $data['home']->text3header }}" />
                      <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                    </form>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 2
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="inputfile">
                      <h5>Section 2 Title</h5>
                      <form action="?action=update_sec2_title_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_title_post" value="true">
                        <input type="text" class="form-control" name="header_title" autocomplete="off" value="{{ $data['home']->titlesection2 }}" />
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <img src="{{ $data['home']->imgsec2_1 }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Set 1</h5>
                      <form action="?action=update_sec2_1_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_post" value="true">
                        <input type="hidden" name="sec" value="1">
                        <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
                        <h5>Title</h5>
                        <textarea class="form-control" id="hed1" name="header_title">{{ $data['home']->textsec2_1 }}</textarea>
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <img src="{{ $data['home']->imgsec2_2 }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Set 2</h5>
                      <form action="?action=update_sec2_2_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_post" value="true">
                        <input type="hidden" name="sec" value="2">
                        <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
                        <h5>Title</h5>
                        <textarea class="form-control" id="hed2" name="header_title">{{ $data['home']->textsec2_2 }}</textarea>
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <img src="{{ $data['home']->imgsec2_3 }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Set 3</h5>
                      <form action="?action=update_sec2_3_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_post" value="true">
                        <input type="hidden" name="sec" value="3">
                        <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
                        <h5>Title</h5>
                        <textarea class="form-control" id="hed3" name="header_title">{{ $data['home']->textsec2_3 }}</textarea>
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <img src="{{ $data['home']->imgsec2_4 }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Set 4</h5>
                      <form action="?action=update_sec2_4_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_post" value="true">
                        <input type="hidden" name="sec" value="4">
                        <input type="file" name="photo" accept=".png" style="margin-bottom: 15px" />
                        <h5>Title</h5>
                        <textarea class="form-control" id="hed4" name="header_title">{{ $data['home']->textsec2_4 }}</textarea>
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>

                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          {{-- <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 3
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4"><img src="{{$data['home']->imgsec3}}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <h5>Section Image</h5>
                    <div class="inputfile">
                      <form action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="add_sec3_photo_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br>
                        <input type="submit" class="btn btn-primary" value="Upload" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <form action="" method="post">
                        <input type="hidden" name="add_sec3_post" value="true">
                        <h5>Section Title</h5>
                        <input type="text" class="form-control" name="header_title" autocomplete="off" value="{{ $data['home']->titlesec3 }}" />
                        <h5>Section Text</h5>
                        <textarea class="form-control" id="hed5" name="header_text">{{ $data['home']->textsec3 }}</textarea>
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                  </div>
                </div>

              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section> --}}

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 3
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4"><img src="{{ $data['home']->imgsec5 }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <h5>Section Image</h5>
                    <div class="inputfile">
                      <form action="?action=add_sec4_photo_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="add_sec4_photo_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br>
                        <input type="submit" class="btn btn-primary" value="Upload" style="margin-top: 15px;" />
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <form action="?action=add_sec4_post" method="post">
                        @csrf
                        <input type="hidden" name="add_sec4_post" value="true">
                        <h5>Section Title</h5>
                        <input type="text" class="form-control" name="header_title" autocomplete="off" value="{{ $data['home']->titlesec5 }}" />
                        <h5>Section Text</h5>
                        <input type="text" class="form-control" name="header_text" autocomplete="off" value="{{ $data['home']->descsec5 }}" />
                        <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                      </form>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


<script>
    var simpleConfig = {
        height: "90px",
        toolbar: [
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline'] },
            { name: 'paragraph', items: ['NumberedList', 'BulletedList'] },
            { name: 'insert', items: ['Link', 'Unlink'] }
        ],
        removePlugins: 'elementspath',
        resize_enabled: false
    };
  var editor = CKEDITOR.replace('hed1', simpleConfig);
  var editor = CKEDITOR.replace('hed2', simpleConfig);
  var editor = CKEDITOR.replace('hed3', simpleConfig);
  var editor = CKEDITOR.replace('hed4', simpleConfig);
  var editor = CKEDITOR.replace('hed5', simpleConfig);

  function get_delete(id){
    $('#id').val(id);
    $('#myModal').modal('show');
  }
</script>


@endsection
