@extends('pages.admin.theme.layout')
@section('title', 'Layouts - Manufacturing')
@section('content')

<style>
    .inputfile{
      padding-bottom: 25px;
    }
</style>
<script src="{{ asset('assets-panel/ckeditor2/ckeditor.js') }}"></script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Layout Manufacturing</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Layout</li>
              <li class="breadcrumb-item active">Manufacturing</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Header
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $data['data']->header_img }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="?action=update_imageheader_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_imageheader_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                  <h5>Header Title</h5>
                  <form method="post" action="?action=update_header_post">
                    @csrf
                    <input type="hidden" name="update_header_post" value="true">
                    <textarea class="form-control" id="hed1" name="header_title">{{ $data['data']->title_heading }}</textarea>
                    <h5>Header Description</h5>
                    <textarea rows="5" class="form-control" id="hed2" name="header_desc">{{ $data['data']->desc_heading }}</textarea>
                    <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                  </form>
                </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 1
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $data['data']->section1_img }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="?action=update_sec1_photo_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec1_photo_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h5>Header Title</h5>
                    <form method="post" action="?action=update_sec1_post">
                        @csrf
                      <input type="hidden" name="update_sec1_post" value="true">
                      <textarea class="form-control" id="hed3" name="header_title">{{ $data['data']->section1_title }}</textarea>
                      <h5>Header Description</h5>
                      <textarea rows="5" class="form-control" id="hed4" name="header_desc">{{ $data['data']->section1_desc }}</textarea>
                      <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                    </form>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 2
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $data['data']->section2_img }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="?action=update_sec2_photo_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec2_photo_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h5>Header Title</h5>
                    <form method="post" action="?action=update_sec2_post">
                        @csrf
                      <input type="hidden" name="update_sec2_post" value="true">
                      <textarea class="form-control" id="hed5" name="header_title">{{ $data['data']->section2_title }}</textarea>
                      <h5>Header Description</h5>
                      <textarea rows="5" class="form-control" id="hed6" name="header_desc">{{ $data['data']->section2_desc }}</textarea>
                      <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                    </form>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Section 3
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $data['data']->section3_img }}" width="300" class="img-thumbnail" />
                  </div>
                  <div class="col-md-8">
                    <div class="inputfile">
                      <h5>Header Image (*.png)</h5>
                      <form action="?action=update_sec3_photo_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="update_sec3_photo_post" value="true">
                        <input type="file" name="photo" accept=".png" /><br><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h5>Header Title</h5>
                    <form method="post" action="?action=update_sec3_post">
                        @csrf
                      <input type="hidden" name="update_sec3_post" value="true">
                      <textarea class="form-control" id="hed7" name="header_title">{{ $data['data']->section3_title }}</textarea>
                      <h5>Header Description</h5>
                      <textarea rows="5" class="form-control" id="hed8" name="header_desc">{{ $data['data']->section3_desc }}</textarea>
                      <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                    </form>
                  </div>

                </div>

              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>


          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<script>
    var simpleConfig = {
        toolbar: [
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline'] },
            { name: 'paragraph', items: ['NumberedList', 'BulletedList'] },
            { name: 'insert', items: ['Link', 'Unlink'] }
        ],
        removePlugins: 'elementspath',
        resize_enabled: false
    };

  var editor = CKEDITOR.replace('hed1', simpleConfig);
  var editor = CKEDITOR.replace('hed2', simpleConfig);
  var editor = CKEDITOR.replace('hed3', simpleConfig);
  var editor = CKEDITOR.replace('hed4', simpleConfig);
  var editor = CKEDITOR.replace('hed5', simpleConfig);
  var editor = CKEDITOR.replace('hed6', simpleConfig);
  var editor = CKEDITOR.replace('hed7', simpleConfig);
  var editor = CKEDITOR.replace('hed8', simpleConfig);

  function get_delete(id){
    $('#id').val(id);
    $('#myModal').modal('show');
  }

  $(document).ready(function(){
    $('#tags').tagsInput({
      'defaultText':'tambah'
    });
  });
</script>

@endsection
