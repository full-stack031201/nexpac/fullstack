@extends('pages.admin.theme.layout')
@section('title', 'Layouts - Contact')
@section('content')

<style>
    .inputfile{
      padding-bottom: 25px;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Layout Contact</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Layout</li>
              <li class="breadcrumb-item active">Contact</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            @if(Session::has('success'))
                <div style="margin: 0 0 10px 10px;"><span class="badge success">{{ Session::get('success') }}</span></div>
            @endif
            @if(Session::has('error'))
                <div style="margin: 0 0 10px 10px;"><span class="badge danger">{{ Session::get('success') }}</span></div>
            @endif
          <!-- Left col -->

          <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-images mr-1"></i>
                  Contact Us
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="col-md-12">
                  <form method="post" action="">
                    @csrf
                    <input type="hidden" name="update_header_post" value="true">
                    <h5>Perusahaan</h5>
                    <input type="text" class="form-control" name="perusahaan" autocomplete="off" value="{{ $data['contact']->perusahaan }}" />
                    <h5>Logo Sansico</h5>
                    <input type="text" class="form-control" name="logo_sansico" autocomplete="off" value="{{ $data['contact']->logo_sansico }}" />
                    <h5>Maps</h5>
                    <input type="text" class="form-control" name="maps" autocomplete="off" value="{{ $data['contact']->maps }}" />
                    <h5>Title Section</h5>
                    <input type="text" class="form-control" name="title_section1" autocomplete="off" value="{{ $data['contact']->title_section1 }}" />
                    <h5>Alamat</h5>
                    <input type="text" class="form-control" name="alamat" autocomplete="off" value="{{ $data['contact']->alamat }}" />
                    <h5>Email</h5>
                    <input type="text" class="form-control" name="email" autocomplete="off" value="{{ $data['contact']->email }}" />
                    <h5>Telp</h5>
                    <input type="text" class="form-control" name="telp" autocomplete="off" value="{{ $data['contact']->telp }}" />
                    <h5>Facebook</h5>
                    <input type="text" class="form-control" name="fb" autocomplete="off" value="{{ $data['contact']->fb }}" />
                    <h5>Instagram</h5>
                    <input type="text" class="form-control" name="ig" autocomplete="off" value="{{ $data['contact']->ig }}" />
                    <h5>Linkedin</h5>
                    <input type="text" class="form-control" name="linkedin" autocomplete="off" value="{{ $data['contact']->linkedin }}" />
                    <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 15px;" />
                  </form>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<script>
  $(document).ready(function(){
    $('#tags').tagsInput({
      'defaultText':'tambah'
    });
  });
</script>

@endsection
