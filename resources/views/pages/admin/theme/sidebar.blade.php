<!-- Main Sidebar Container -->
@php
    isset($data['mod']) ? $mod = $data['mod'] : $mod = '';
    isset($data['page']) ? $page = $data['page'] : $page = '';
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="{{ asset('assets-panel/assets/img/logo.png') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">NexPac</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('assets-panel/assets/img/user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ route('dashboard') }}" class="d-block">{{ \Str::upper(auth()->user()->username) }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item <?php if ($mod=="master") echo 'menu-is-opening menu-open' ?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Master
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('master.login.index') }}" class="nav-link <?php if ($mod=="master" && $page=="login") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Login</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('master.application.index') }}" class="nav-link <?php if ($mod=="master" && $page=="application") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Application</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('master.product.index') }}" class="nav-link <?php if ($mod=="master" && $page=="product") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if ($mod=="layout") echo 'menu-is-opening menu-open' ?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Layouts
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('layout.home.index') }}" class="nav-link <?php if ($mod=="layout" && $page=="home") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('layout.about.index') }}" class="nav-link <?php if ($mod=="layout" && $page=="about") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About Us</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('layout.manufacturing.index') }}" class="nav-link <?php if ($mod=="layout" && $page=="manufacturing") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manufacturing</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('layout.news.index') }}" class="nav-link <?php if ($mod=="layout" && $page=="news") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>News</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('layout.contact.index') }}" class="nav-link <?php if ($mod=="layout" && $page=="contact") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item <?php if ($mod=="news") echo 'menu-is-opening menu-open' ?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                News
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('news.create') }}" class="nav-link <?php if ($mod=="news" && $page=="add_post") echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Post</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('news.index') }}" class="nav-link <?php if ($mod=="news" && ($page=="view_post" || $page=="edit_post")) echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View Post</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
