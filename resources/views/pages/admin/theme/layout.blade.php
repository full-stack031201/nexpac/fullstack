<!DOCTYPE html>
<html lang="en">
<head>
    @include('pages.admin.theme.meta')

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('pages.admin.theme.header')
        @include('pages.admin.theme.sidebar')

        @yield('content')

        @include('pages.admin.theme.footer')
    </div>

    @include('pages.admin.theme.script')
    @stack('script')
</body>
</html>
