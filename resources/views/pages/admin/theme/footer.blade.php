<footer class="main-footer">
    <strong>Copyright &copy; <?= date("Y"); ?> NexPac.</strong>All rights reserved.
    <div class="float-right d-none d-sm-inline-block">  </div>
</footer>
