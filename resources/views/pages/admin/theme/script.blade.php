<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets-panel/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/jquery.tagsinput/jquery.tagsinput.min.js') }}"></script>
<script src="{{ asset('assets-panel/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets-panel/assets/js/adminlte.js') }}"></script>
