<style type="text/css">
</style>
@php
    $contact = \App\Models\Contact::first();
@endphp
<div class="">
	<div class="mx-lg-4 mx-md-3 mx-2 py-3" style="margin-top: 60px;">
		<footer class="d-md-flex align-items-center text-md-start text-center text-tersier-nexpac">
			<div class="col-md-9">
				<h4 style="margin: 0;margin-bottom: 10px;">{{ $contact->perusahaan }}</h4>
				<small>Part of <img src="{{ $contact->logo_sansico }}" width="100px"></small><br>
				<small>{{ $contact->alamat }}</small>
			</div>
			<hr class="my-3 d-md-none">
			<div class="col-md-3 text-md-end">
				<h3>
					<a href="{{ $contact->fb }}" class="text-dark"><i class="fab fa-facebook mx-md-1 mx-2"></i></a>
					<a href="{{ $contact->ig }}" class="text-dark"><i class="fab fa-instagram mx-md-1 mx-2"></i></a>
					<a href="{{ $contact->linkedin }}" class="text-dark"><i class="fab fa-linkedin mx-md-1 mx-2"></i></a>
				</h3>
			</div>
		</footer>
	</div>
</div>
