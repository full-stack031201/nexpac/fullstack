<h3>Hello, {{ $firstname }} {{ $lastname }}</h3>
<p>First Name : {{ $firstname }}</p>
<p>Last Name : {{ $lastname }}</p>
<p>Email : {{ $email }}</p>
<p>Phone : {{ $phone }}</p>
<p>Company : {{ $company }}</p>
<p>Position : {{ $position }}</p>
<p>Thanks for contacting us</p><br>