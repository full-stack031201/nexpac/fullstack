<script src="https://www.google.com/recaptcha/api.js"></script>
<section class="text-center mt-3 mt-md-5">
    <div>
        <div class="bg-blue py-3 contact-us" data-aos="fade-down" style="padding-bottom: 20px !important;">
            <br><br>
            <h1 class="text-white mb-lg-4 mb-2" style="font-weight: normal;">CONTACT US</h1>
            <div class="text-start text-white" id="submitform">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mx-md-5 mt-3 me-md-4">
                            <label>First Name</label>
                            <input type="text" name="firstname" id="firstname" class="w-100 bg-primary-nexpac text-white" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mx-md-5 mt-3 ms-md-4">
                            <label>Last Name</label>
                            <input type="text" name="lastname" id="lastname" class="w-100 bg-primary-nexpac text-white" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mx-md-5 mt-3 me-md-4">
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="w-100 bg-primary-nexpac text-white" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mx-md-5 mt-3 ms-md-4">
                            <label>Phone</label>
                            <input type="number" name="telephone" id="telephone" class="w-100 bg-primary-nexpac text-white" required>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="form-group mx-md-5 mt-3">
                        <label>Company</label>
                        <input type="text" name="company" id="company" class="w-100 bg-primary-nexpac text-white" required>
                    </div>
                    <div class="form-group mx-md-5 mt-3">
                        <label>Position</label>
                        <input type="text" name="position" id="position" class="w-100 bg-primary-nexpac text-white" required>
                    </div>
                </div>
                <div class="form-group mt-3 mx-md-4 d-flex align-items-center justify-content-center">
                    <div id="captcha-contact" class="g-recaptcha brochure__form__captcha" data-sitekey="{{ base64_decode('NkxlNG1fc3BBQUFBQVA4eVpkMFk2UUx5UVAyS3gtazBpU1JNLVdjdg==') }}"></div>
                </div>
                <div class="form-group mx-md-5 mt-3 text-center">
                    <br>
                    <button class="btn btn-light" id="submit">
                        <strong>
                            <div class="spinner-border d-none" id="loading" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                            <span id="submit-teks">SUBMIT</span>
                        </strong>
                    </button><br>
                    <small id="message-success" class="d-none">Thanks for submitting!</small>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#submit').click(function(e){
        var resCaptch = grecaptcha.getResponse();
        if(resCaptch.length == 0){
            alert("please verify you are humann!");
            return false;
        }

        $('#cover-roll').removeClass('d-none')
        e.preventDefault();
        $('#loading').removeClass('d-none')
        $('#submit-teks').addClass('d-none')
        let firstname = $('#firstname').val();
        let lastname = $('#lastname').val();
        let email = $('#email').val();
        let telephone = $('#telephone').val();
        let company = $('#company').val();
        let position = $('#position').val();
        let token   = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            url: "{{ route('contact.store') }}",
            type: 'post',
            cache: false,
            data: {
                'firstname': firstname,
                'lastname': lastname,
                'email': email,
                'telephone': telephone,
                'company': company,
                'position': position,
                '_token': "{{ csrf_token() }}"
            },
            success: function(data){
                $('#message-success').removeClass('d-none');
                $('#submit-teks').removeClass('d-none')
                $('#message-success').html('Thanks for submitting!');
                $('#firstname').val('');
                $('#lastname').val('');
                $('#email').val('');
                $('#telephone').val('');
                $('#company').val('');
                $('#position').val('');
                $('#cover-roll').addClass('d-none')
                $('#loading').addClass('d-none')
                grecaptcha.reset();
            },
            error: function(xhr){
                console.log(xhr)
                $('#message-success').removeClass('d-none')
                $('#submit-teks').removeClass('d-none')
                $('#message-success').html('<br><span class="alert alert-danger">Please fill all data</span><br>')
                $('#cover-roll').addClass('d-none')
                $('#loading').addClass('d-none')
                grecaptcha.reset();
            }
        });

    })
</script>
