<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="modal-body">
    <form method="post" action="{{ route('product.foodmail') }}"  onsubmit="return submitPaper();">
        @csrf
        <div class="d-lg-flex align-items-center">
            <div class="col-lg-4">
                <img width="100%" id="image-food">
            </div>
            <div class="col-lg-8 py-2 px-3" style="background-color: rgb(179, 179, 179);">
                <div class="row">
                    <div class="col-md-6">
                        <small class="text-white">Food & Beverage Paper Packaging</small><br>
                        <h3 class="text-blue" id="text-food"></h3>
                        <input type="hidden" name="product_name" id="name_food">
                        <div class="position-relative">
                            <i class="fas text-light-gray fa-caret-down position-absolute" style="top: 50%; transform: translateY(-50%); right: 10px; font-size: 1.5rem;"></i>
                            <select id="select-value" class="form-control" name="size">
                            </select>
                        </div>
                        <style type="text/css">
                            .jenis-kertas:checked + .bg-paper{
                                box-shadow: 2.5px 2.5px 5px 1px #000;
                            }
                        </style>
                        <div class="d-flex my-2 align-items-center text-white">
                            <div class="form-group me-2">
                                <label class="d-flex align-items-center">
                                    <input required type="radio" name="color" value="Ivory 300gsm" class="d-none jenis-kertas" checked>
                                    <div class="bg-paper bg-white" style="width: 40px; height: 40px"></div>
                                    <div class="text-food-small">
                                        <small class="ms-2">Ivory</small>
                                    </div>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="d-flex align-items-center">
                                    <input required type="radio" name="color" value="Ecokraft" class="d-none jenis-kertas">
                                    <div class="bg-paper" style="width: 40px; height: 40px; background-color: brown;"></div>
                                    <div class="text-food-small">
                                        <small class="ms-2">Ecokraft</small>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div id="description" class="text-white my-2"></div>
                        <div class="form-group my-2">
                            <label class="text-white">Amount Order</label>
                            <div class="input-group">
                                <span id="minus-order" class="input-group-text"><i class="fas fa-minus"></i></span>
                                  <input type="number" class="form-control bg-white" min="300000" max="500000" value="300000" name="amount" id="order">
                                  <span id="plus-order" class="input-group-text"><i class="fas fa-plus"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mt-2">
                            <label class="text-white">Name</label>
                              <input type="text" class="form-control text-white" name="name" required style="background: none; outline: none; border: none; border-bottom: 1px solid white;">
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-white">Email</label>
                              <input type="email" class="form-control text-white" name="email" required style="background: none; outline: none; border: none; border-bottom: 1px solid white;">
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-white">Phone</label>
                              <input type="text" class="form-control text-white" name="phone" required style="background: none; outline: none; border: none; border-bottom: 1px solid white;">
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-white">Company and Position</label>
                              <input type="text" class="form-control text-white" name="company" required style="background: none; outline: none; border: none; border-bottom: 1px solid white;">
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 mx-md-4 d-flex align-items-center justify-content-center">
                    <div id="captcha-paper" class="g-recaptcha brochure__form__captcha" data-sitekey="{{ base64_decode('NkxlNG1fc3BBQUFBQVA4eVpkMFk2UUx5UVAyS3gtazBpU1JNLVdjdg==') }}"></div>
                </div>
                <div class="form-group my-2">
                    <button class="btn btn-primary w-100">SUBMIT</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function submitPaper(){
            var resCaptch = grecaptcha.getResponse();
            if(resCaptch.length == 0){
                alert("please verify you are humann!");
                return false;
            }
        }
</script>
