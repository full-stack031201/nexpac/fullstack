@extends('pages.theme.layout')
@section('tittle', 'About Us')
@section('content')
    <style type="text/css">
        @media screen and (max-width: 464px){
            .overview{
                margin: 100px 10px 0 10px;
            }
            h1.tittle-home{
                font-size: 30px;
                font-weight: normal;
            }
            .overview span{
                font-size: 12px;
                line-height: 15px;
            }
        }
        @media screen and (max-width: 767px) and (min-width: 465px){
            .overview{
                margin: 100px 10px 0 10px;
                line-height: 18px;
            }
            h1.tittle-home{
                font-size: 40px;
                font-weight: normal;
            }
            .overview span{
                font-size: 13px;
                line-height: 18px;
            }
        }
        @media screen and (max-width: 991px) and (min-width: 768px){
            .overview{
                margin: 100px 50px 0 50px;
            }
            h1.tittle-home{
                font-size: 45px;
                font-weight: normal;
            }
            .overview span{
                font-size: 13px;
                line-height: 18px;
            }
        }
        @media screen and (min-width: 992px){
            .overview{
                margin: 100px 50px 0 50px;
            }
            h1.tittle-home{
                font-size: 50px;
                font-weight: normal;
            }
        }
    </style>

    <section class="overview">
        <div class="container">
            <div class="mx-lg-5 px-lg-5">
                <div class="d-md-flex align-items-center">
                    <div class="d-md-none text-center mb-3">
                        <img src="{{ $about->section1_img }}" width="80%">
                    </div>
                    <div class="col-lg-7 col-md-7 text-center text-gray">
                        <div class="mx-md-2">
                            <h1 class="text-blue mb-md-3 mb-2 tittle-home">
                                <?= ''.$about->section1_title ?>
                            </h1>
                            <span>
                                <?= ''.$about->section1_desc ?>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-5 d-md-block d-none">
                        <img src="{{ $about->section1_img }}" width="100%">
                    </div>
                </div>
            </div>
        </div>
    </section>
	<section class="overview">
		<div class="container">
			<div class="text-center text-gray" data-aos="fade-down">
				<h1 class="text-blue mb-md-3 mb-2 tittle-home">
					<?= ''.$about->section2_title ?>
				</h1>
				<span>
					<?= ''.$about->section2_desc ?>
				</span>
			</div>
		</div>
	</section>
	<section class="overview">
		<div class="container">
			<div class="text-center text-gray" data-aos="fade-down">
				<h1 class="text-blue mb-md-3 mb-2 tittle-home">
					<?= ''.$about->section3_title ?>
				</h1>
				<span>
					<?= ''.$about->section3_desc ?>
				</span>
			</div>
		</div>
	</section>
	<section class="text-center overview">
		<div class="certification">
			<div class="mx-md-5 mx-3 py-3" style="border-bottom-width: 20px;">
				<div class="mx-md-5 mx-3 py-3">
					<h1 class="tittle-home text-blue" data-aos="fade-down" style="margin:0"><?= ''.$about->section4_title ?></h1>
					<span class="text-gray" data-aos="fade-down"><?= ''.$about->section4_desc ?></span>
						<div>
						<div class="content-header d-flex align-items-center justify-content-center overflow-hidden">
							<img data-aos="fade-down" src="{{ $about->section5_img }}" width="80%" style="margin-top: 55px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		document.getElementsByClassName('menu-top')[1].classList.add('active');
	</script>
@endsection
