<p>Dear our customer, {{ $name }}</p>
<p>Thank you for choosing nexpac for your packaging.</p>
<p>Our team will contact you soon and help according to your product packaging needs.</p><br>
<p>Product Name : {{ $product_name }}</p>
<p>Size : {{ $size }}</p>
<p>Amount Order : {{ $amount }}</p>
<p>Best regards,</p>
<p><strong>Nexpac Aseptic Indonesia</strong></p>