@extends('pages.theme.layout')
@section('tittle', 'Contact Us')
@section('content')
	<style type="text/css">
		@media screen and (min-width: 992px){
			.content-header{
				position: relative;
				height: 60vh;
				background-size: 100% auto;
			}
			.find-us iframe{
				width: 100%;
				height: 450px;
			}
		}
		@media screen and (max-width: 991px) and (min-width: 768px){
			.find-us iframe{
				width: 100%;
				height: 300px;
			}
		}
		@media screen and (max-width: 767px){
			.find-us iframe{
				width: 100%;
				height: 250px;
			}
		}
		.contact-us input{
			background-color: #034e9e;
			border: none;
			border-bottom: 1px solid #fff;
		}
		.contact-us input:focus{
			outline:0px !important;
			background-color: #034e9e
		}
	</style>
    <script src="https://www.google.com/recaptcha/api.js"></script>
	<section class="content-header d-flex align-items-center justify-content-center overflow-hidden">
		<img src="{{ $contact->header_img }}" width="100%">
	</section>
	<section class="text-center">
		<div>
			<div class="bg-blue py-3 contact-us" data-aos="fade-down">
				<br><br>
				<h1 class="text-white mb-lg-4 mb-2" style="font-weight: normal;">CONTACT US</h1>
				<div class="text-start text-white" id="submitform">
					<div class="row">
						<div class="col-6">
							<div class="form-group mx-md-4 mt-3 me-md-4">
								<label>First Name</label>
								<input type="text" name="firstname" id="firstname" class="w-100 bg-blue text-white">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group mx-md-4 mt-3 ms-md-4">
								<label>Last Name</label>
								<input type="text" name="lastname" id="lastname" class="w-100 bg-blue text-white">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<div class="form-group mx-md-4 mt-3 me-md-4">
								<label>Email</label>
								<input type="email" name="email" id="email" class="w-100 bg-blue text-white">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group mx-md-4 mt-3 ms-md-4">
								<label>Phone</label>
								<input type="number" name="telephone" id="telephone" class="w-100 bg-blue text-white">
							</div>
						</div>
					</div>
					<div>
						<div class="form-group mx-md-4 mt-3">
							<label>Company</label>
							<input type="text" name="company" id="company" class="w-100 bg-blue text-white">
						</div>
						<div class="form-group mx-md-4 mt-3">
							<label>Position</label>
							<input type="text" name="position" id="position" class="w-100 bg-blue text-white">
						</div>
					</div>
                    <div class="form-group mt-3 mx-md-4 d-flex align-items-center justify-content-center">
                        <div class="g-recaptcha brochure__form__captcha" data-sitekey="6LdpNvwpAAAAACgjGO3KTaYJW6SXmuzogLOPwaoc"></div>
                    </div>
					<div class="form-group mx-md-4 mt-3 text-center">
						<br>
						<button class="btn btn-light px-5" id="submit">
                            <strong>
                                <div class="spinner-border d-none" id="loading" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                                <span id="submit-teks">SUBMIT</span>
                            </strong>
                        </button><br>
						<small id="message-success" class="d-none">Thanks for submitting!</small>
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</section>
    <section class="my-md-5 my-5 find-us">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-5" data-aos="fade-right">
					<div class="border-blue border-5">
						<iframe src="{{ $contact->maps }}" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
					</div>
				</div>
				<div class="col-lg-6 col-md-7" data-aos="fade-left">
					<div class="mx-md-4 text-blue">
						<h1 style="font-weight: normal;">{{ $contact->title_section1 }}</h1>
						<div class="group-find-us">
							<h5>Alamat</h5>
							<span class="text-gray">{{ $contact->alamat }}</span>
						</div>
						<div class="group-find-us mt-lg-3 mt-2">
							<h5>Email</h5>
							<span class="text-gray">{{ $contact->email }}</span>
						</div>
						<div class="group-find-us mt-lg-3 mt-2">
							<h5>Call</h5>
							<span class="text-gray">{{ $contact->telp }}</span>
                            @if ($contact->fb)
                                <a href="{{ $contact->fb }}" target="__blank" class="text-decoration-none text-blue">
                                    <div class="group-find-us mt-lg-3 mt-2 d-flex align-items-center">
                                        <h2 style="margin: 0"><i class="fab fa-facebook me-3"></i></h2>
                                        <span class="text-gray"> Nexpac</span>
                                    </div>
                                </a>
                            @endif
                            @if ($contact->ig)
                                <a href="{{ $contact->ig }}" target="__blank" class="text-decoration-none text-blue">
                                    <div class="group-find-us mt-md-1 mt-1 d-flex align-items-center">
                                        <h2 style="margin: 0"><i class="fab fa-instagram me-3"></i></h2>
                                        <span class="text-gray ms-1"> Nexpac.id</span>
                                    </div>
                                </a>
                            @endif
                            @if ($contact->linkedin)
                                <a href="{{ $contact->linkedin }}" target="__blank" class="text-decoration-none text-blue">
                                    <div class="group-find-us mt-md-1 mt-1 d-flex align-items-center">
                                        <h2 style="margin: 0"><i class="fab fa-linkedin me-3"></i></h2>
                                        <span class="text-gray ms-1">Nexpac Aseptik Indonesia</span>
                                    </div>
                                </a>
                            @endif
					</div>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		document.getElementsByClassName('menu-top')[5].classList.add('active');
		$('#submit').click(function(e){
            var resCaptch = grecaptcha.getResponse();
            if(resCaptch.length == 0){
                alert("please verify you are humann!");
                return false;
            }

			$('#cover-roll').removeClass('d-none')
			e.preventDefault();
            $('#loading').removeClass('d-none')
            $('#submit-teks').addClass('d-none')
			let firstname = $('#firstname').val();
			let lastname = $('#lastname').val();
			let email = $('#email').val();
			let telephone = $('#telephone').val();
			let company = $('#company').val();
			let position = $('#position').val();
			let token   = $("meta[name='csrf-token']").attr("content");

			$.ajax({
				url: "{{ route('contact.store') }}",
				type: 'post',
				cache: false,
				data: {
					'firstname': firstname,
					'lastname': lastname,
					'email': email,
					'telephone': telephone,
					'company': company,
					'position': position,
					'_token': "{{ csrf_token() }}",
				},
				success: function(data){
                    $('#loading').addClass('d-none')
                    $('#submit-teks').removeClass('d-none')
					$('#message-success').removeClass('d-none');
					$('#message-success').html('Thanks for submitting!');
					$('#firstname').val('');
					$('#lastname').val('');
					$('#email').val('');
					$('#telephone').val('');
					$('#company').val('');
					$('#position').val('');
					$('#cover-roll').addClass('d-none')
                    grecaptcha.reset();
				},
				error: function(xhr){
					console.log(xhr)
                    $('#loading').addClass('d-none')
                    $('#submit-teks').removeClass('d-none')
					$('#message-success').removeClass('d-none')
					$('#message-success').html('<br><span class="alert alert-danger">Please fill all data</span><br>')
					$('#cover-roll').addClass('d-none')
                    grecaptcha.reset();
				}
			});

		})
	</script>
	@if(Session::has('successContactUs'))
	<script type="text/javascript">
		iziToast.success({
            title : "Success",
            message: "Thanks for submitting !",
            position: "topCenter"
        })
	</script>
	@endif
@endsection
