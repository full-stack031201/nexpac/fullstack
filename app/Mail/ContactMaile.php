<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ContactMaile extends Mailable
{
    use Queueable, SerializesModels;

    public $firstname, $lastname, $email, $phone, $company, $position;

    public function __construct($firstname, $lastname, $email, $phone, $company, $position)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->phone = $phone;
        $this->company = $company;
        $this->position = $position;
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Thank you for contacting Nexpac',
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'pages.contactmail',
            with: [
                'firstname' => $this->firstname,
                'lastname' => $this->lastname,
                'email' => $this->email,
                'phone' => $this->phone,
                'company' => $this->company,
                'position' => $this->position
            ],
        );
    }

    public function attachments(): array
    {
        return [];
    }
}
