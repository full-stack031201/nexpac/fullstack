<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use DB, Storage;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $newsData = News::orderByDesc('id')->get();
        $data = [
            'news' => $newsData,
            'mod' => 'news',
            'page' => 'view_post',
        ];
        return view('pages.admin.news.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [
            'mod' => 'news',
            'page' => 'add_post',
        ];
        return view('pages.admin.news.create', compact(['data']));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $tujuan_upload = public_path('assets/images/news/');
            $file = $request->file('feature_image');
            $fileName = time().'-'.$file->getClientOriginalName();
            $file->move($tujuan_upload, $fileName);

            $feature_image = url('assets/images/news/'.$fileName);

            $data = [
                'title' => $request->title,
                'feature_image' => $feature_image,
                'author' => auth()->user()->username,
                'content' => $request->content,
                'news_created' => now(),
            ];

            News::firstOrCreate($data);

            DB::commit();
            return redirect()->route('news.index')->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $newsData = News::find($id);
        $data = [
            'news' => $newsData,
            'mod' => 'news',
            'page' => 'view_post',
        ];
        return view('pages.admin.news.edit', compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $newsData = News::find($id);
        try {
            DB::beginTransaction();
            if($request->feature_image){
                $tujuan_upload = public_path('assets/images/news/');
                $file = $request->file('feature_image');
                $fileName = time().'-'.$file->getClientOriginalName();
                $file->move($tujuan_upload, $fileName);

                $feature_image = url('assets/images/news/'.$fileName);
            } else {
                $feature_image = $newsData->feature_image;
            }

            $data = [
                'title' => $request->title,
                'feature_image' => $feature_image,
                'content' => $request->content,
            ];

            News::find($id)->update($data);

            DB::commit();
            return redirect()->route('news.index')->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = News::find($id)->delete();
        return response()->json(['message' => 'data deleted', 'data' => $data], 200);
    }

    public function upload_image(Request $request)
    {
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $filePath = 'assets/news/uploads/' . $fileName;

            // Simpan file ke storage/public/uploads
            Storage::put('public/' . $filePath, file_get_contents($file));

            $url = Storage::url($filePath);

            return response()->json([
                'url' => $url
            ]);
        }

        return response()->json(['error' => 'No file uploaded'], 400);
    }
}
