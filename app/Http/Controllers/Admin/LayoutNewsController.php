<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NewsCover;
use DB;

class LayoutNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $news = NewsCover::first();
        $data = [
            'mod' => 'layout',
            'page' => 'news',
            'news' => $news
        ];
        return view('pages.admin.layout.news.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $act = $request->action;
            $about = NewsCover::first();

            if($act){
                $data = [];
                if($act == 'update_imageheader_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/news/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/news/'.$fileName);

                        $data = ['header_img' => $photo];

                    }
                } elseif($act == 'update_header_post'){
                    $data = [
                        'title_heading' => $request->header_title,
                    ];
                }
                NewsCover::find($about->id)->update($data);
            }

            DB::commit();
            return back()->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
