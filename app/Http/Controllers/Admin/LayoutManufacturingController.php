<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Manufacturing;
use DB;

class LayoutManufacturingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Manufacturing::first();
        $data = [
            'mod' => 'layout',
            'page' => 'manufacturing',
            'data' => $data
        ];
        return view('pages.admin.layout.manufacturing.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $act = $request->action;
            $manufactur = Manufacturing::first();

            if($act){
                $data = [];

                if($act == 'update_imageheader_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/manufacturing/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/manufacturing/'.$fileName);

                        $data = ['header_img' => $photo];

                    }
                } elseif($act == 'update_header_post'){
                    $data = [
                        'title_heading' => $request->header_title,
                        'desc_heading' => $request->header_desc,
                    ];
                } elseif($act == 'update_sec1_photo_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/manufacturing/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/manufacturing/'.$fileName);

                        $data = ['section1_img' => $photo];

                    }
                } elseif($act == 'update_sec1_post'){
                    $data = [
                        'section1_title' => $request->header_title,
                        'section1_desc' => $request->header_desc,
                    ];
                } elseif($act == 'update_sec2_photo_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/manufacturing/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/manufacturing/'.$fileName);

                        $data = ['section2_img' => $photo];

                    }
                } elseif($act == 'update_sec2_post'){
                    $data = [
                        'section2_title' => $request->header_title,
                        'section2_desc' => $request->header_desc,
                    ];
                } elseif($act == 'update_sec3_photo_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/manufacturing/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/manufacturing/'.$fileName);

                        $data = ['section3_img' => $photo];

                    }
                } elseif($act == 'update_sec3_post'){
                    $data = [
                        'section3_title' => $request->header_title,
                        'section3_desc' => $request->header_desc,
                    ];
                }

                Manufacturing::find($manufactur->id)->update($data);
            }

            DB::commit();
            return back()->with('success', 'Data saved successfully');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
