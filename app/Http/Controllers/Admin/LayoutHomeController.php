<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home;

use DB;

class LayoutHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $home = Home::first();
        $data = [
            'mod' => 'layout',
            'page' => 'home',
            'home' => $home
        ];
        return view('pages.admin.layout.home.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $act = $request->action;
            $home = Home::first();

            if($act) {
                $data = [];

                if($act == 'update_imageheader_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);

                        $data = ['imageheader' => $photo];

                    }
                } elseif($act == 'update_header_post'){
                    $data = [
                        'text1header' => $request->header1,
                        'text2header' => $request->header2,
                        'text3header' => $request->header3,
                    ];
                } elseif($act == 'update_sec2_title_post'){
                    $data = ['titlesection2' => $request->header_title];
                } elseif($act == 'update_sec2_1_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);
                    } else {
                        $photo = $home->imgsec2_1;
                    }

                    $data = [
                        'imgsec2_1' => $photo,
                        'textsec2_1' => $request->header_title,
                    ];
                } elseif($act == 'update_sec2_2_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);
                    } else {
                        $photo = $home->imgsec2_2;
                    }

                    $data = [
                        'imgsec2_2' => $photo,
                        'textsec2_2' => $request->header_title,
                    ];
                } elseif($act == 'update_sec2_3_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);
                    } else {
                        $photo = $home->imgsec2_3;
                    }

                    $data = [
                        'imgsec2_3' => $photo,
                        'textsec2_3' => $request->header_title,
                    ];
                } elseif($act == 'update_sec2_4_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);
                    } else {
                        $photo = $home->imgsec2_4;
                    }

                    $data = [
                        'imgsec2_4' => $photo,
                        'textsec2_4' => $request->header_title,
                    ];
                } elseif($act == 'add_sec4_photo_post'){
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/home/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/home/'.$fileName);

                        $data = ['imgsec5' => $photo];

                    }
                } elseif($act == 'add_sec4_post') {
                    $data = [
                        'titlesec5' => $request->header_title,
                        'descsec5' => $request->header_text,
                    ];
                }

                Home::find($home->id)->update($data);
            }

            DB::commit();
            return back()->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
