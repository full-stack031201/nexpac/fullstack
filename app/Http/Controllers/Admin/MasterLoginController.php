<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class MasterLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $admin = Admin::orderByDesc('id')->get();
        $data = [
            'mod' => 'master',
            'page' => 'login',
            'admin' => $admin
        ];
        return view('pages.admin.master.login.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:admins,username',
            'pass' => 'required',
            'status' => 'required'
        ]);
        try {
            \DB::beginTransaction();

            $data = [
                'username' => $request->username,
                'password' => \Hash::make($request->pass),
                'status' => $request->status
            ];

            Admin::firstOrCreate($data);

            \DB::commit();

            return back()->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            \DB::rollback();
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Admin::find($id);
        return response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            \DB::beginTransaction();
            if(!isset($request->pass_1) || !$request->pass_1){
                $data = [
                    'status' => $request->status
                ];
            } else  {
                $request->validate([
                    'pass_1' => 'required',
                    'pass_2' => 'required'
                ]);
                $data = [
                    'password' => \Hash::make($request->pass_1)
                ];
            }

            Admin::find($id)->update($data);

            \DB::commit();

            return back()->with('success', 'Data saved successfully');

        } catch (\Throwable $th) {
            \DB::rollback();
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $delete = Admin::find($id)->delete();
            if($delete){
                return response()->json(['message' => 'data deleted'], 200);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }
}
