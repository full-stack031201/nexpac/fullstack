<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ApplicationCategories;
use App\Models\Application;

use DB;

class MasterApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $category = ApplicationCategories::get();
        $application = Application::orderByDesc('application_id')->get();
        $data = [
            'mod' => 'master',
            'page' => 'application',
            'category' => $category,
            'application' => $application
        ];
        return view('pages.admin.master.application.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            if(isset($request->aseptic_photo)){
                $tujuan_upload = public_path('assets/images/packaging/');
                $file = $request->file('aseptic_photo');
                $fileName = time().'-'.$file->getClientOriginalName();
                $file->move($tujuan_upload, $fileName);

                $header_img = url('assets/images/packaging/'.$fileName);

                $category = ApplicationCategories::get()[0];
                $update = ApplicationCategories::find($category->id)->update([
                    'header_img' => $header_img,
                ]);

            } elseif(isset($request->paper_photo)){
                $tujuan_upload = public_path('assets/images/packaging/');
                $file = $request->file('paper_photo');
                $fileName = time().'-'.$file->getClientOriginalName();
                $file->move($tujuan_upload, $fileName);

                $header_img = url('assets/images/packaging/'.$fileName);

                $category = ApplicationCategories::get()[1];
                $update = ApplicationCategories::find($category->id)->update([
                    'header_img' => $header_img,
                ]);

            } else {
                $act = $request->action;

                if($act == 'add_data'){
                    $request->validate([
                        'photo' => 'required',
                        'category' => 'required',
                        'about' => 'required',
                        'spec' => 'required',
                        'name' => 'required'
                    ]);

                    $tujuan_upload = public_path('assets/images/packaging/');
                    $file = $request->file('photo');
                    $fileName = time().'-'.$file->getClientOriginalName();
                    $file->move($tujuan_upload, $fileName);

                    $header_img = url('assets/images/packaging/'.$fileName);

                    $data = [
                        'application_picture' => $header_img,
                        'application_name' => $request->name,
                        'application_spec' => $request->spec,
                        'application_about' => $request->about,
                        'application_category' => $request->category,
                    ];
                    Application::firstOrCreate($data);
                } elseif($act = 'edit_data') {
                    $data = [];
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/packaging/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $header_img = url('assets/images/packaging/'.$fileName);

                        $data = [
                            'application_picture' => $header_img,
                            'application_name' => $request->name,
                            'application_spec' => $request->spec,
                            'application_about' => $request->about,
                            'application_category' => $request->category,
                        ];
                    } else {
                        $data = [
                            'application_name' => $request->name,
                            'application_spec' => $request->spec,
                            'application_about' => $request->about,
                            'application_category' => $request->category,
                        ];
                    }
                    Application::find($request->id)->update($data);
                }

            }

            DB::commit();
            return back()->with('success', 'Data saved successfully');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Application::find($id);
        return response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Application::find($id)->delete();
        return response()->json(['message' => 'data deleted', 'data' => $data], 200);
    }
}
