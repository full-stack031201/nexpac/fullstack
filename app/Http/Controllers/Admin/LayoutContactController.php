<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use DB;

class LayoutContactController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contact = Contact::first();
        $data = [
            'mod' => 'layout',
            'page' => 'contact',
            'contact' => $contact
        ];
        return view('pages.admin.layout.contact.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $contact = Contact::first();
            $data = [
                'perusahaan' => $request->perusahaan,
                'logo_sansico' => $request->logo_sansico,
                'maps' => $request->maps,
                'title_section1' => $request->title_section1,
                'alamat' => $request->alamat,
                'email' => $request->email,
                'telp' => $request->telp,
                'fb' => $request->fb,
                'ig' => $request->ig,
                'linkedin' => $request->linkedin,
            ];

            Contact::find($contact->id)->update($data);
            DB::commit();
            return back()->with('success', 'Data saved successfully');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
