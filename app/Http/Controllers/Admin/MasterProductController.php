<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCover;

use DB;

class MasterProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cover = ProductCover::first();
        $product = Product::orderByDesc('product_id')->get();
        $data = [
            'mod' => 'master',
            'page' => 'product',
            'cover' => $cover,
            'product' => $product
        ];
        return view('pages.admin.master.product.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            if(isset($request->header_img)){
                $tujuan_upload = public_path('assets/images/products/');
                $file = $request->file('header_img');
                $fileName = time().'-'.$file->getClientOriginalName();
                $file->move($tujuan_upload, $fileName);

                $header_img = url('assets/images/products/'.$fileName);

                $category = ProductCover::first();
                $update = ProductCover::find($category->id)->update([
                    'header_img' => $header_img,
                ]);
            } else {
                $act = $request->action;
                if($act == 'add_data'){
                    $tujuan_upload = public_path('assets/images/products/');
                    $file = $request->file('photo');
                    $fileName = time().'-'.$file->getClientOriginalName();
                    $file->move($tujuan_upload, $fileName);

                    $photo = url('assets/images/products/'.$fileName);

                    $data = [
                        'product_name' => $request->name,
                        'product_size' => $request->size,
                        'product_category' => $request->category,
                        'product_picture' => $photo,
                        'product_about' => $request->about,
                    ];

                    Product::firstOrCreate($data);
                } elseif($act == 'edit_data'){
                    $data = [];
                    if($request->photo){
                        $tujuan_upload = public_path('assets/images/products/');
                        $file = $request->file('photo');
                        $fileName = time().'-'.$file->getClientOriginalName();
                        $file->move($tujuan_upload, $fileName);

                        $photo = url('assets/images/products/'.$fileName);

                        $data = [
                            'product_name' => $request->name,
                            'product_size' => $request->size,
                            'product_category' => $request->category,
                            'product_picture' => $photo,
                            'product_about' => $request->about,
                        ];
                    } else {
                        $data = [
                            'product_name' => $request->name,
                            'product_size' => $request->size,
                            'product_category' => $request->category,
                            'product_about' => $request->about,
                        ];
                    }
                    Product::find($request->id)->update($data);
                }
            }

            DB::commit();
            return back()->with('success', 'Data saved successfully');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Product::find($id);
        return response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Product::find($id)->delete();
        return response()->json(['message' => 'data deleted', 'data' => $data], 200);
    }
}
