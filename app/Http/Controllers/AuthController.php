<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Session, Cookie, Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        if(Auth::user()){
            return redirect('/dashboard');
        }
        return view('pages.auth.login');
    }
    public function login_process(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){
            $request->session()->regenerate();
            return redirect()->intended('/dashboard');
        } else {
            return back()->with('error', 'Login gagal. Coba lagi.');
        }
    }
    public function logout_process(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
