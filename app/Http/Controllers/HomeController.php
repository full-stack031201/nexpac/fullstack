<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Certification;
use App\Models\InstagramContent;
use App\Models\ContactUs;
use App\Models\Application;
use App\Models\Home;
use App\Models\AboutUs;
use App\Models\Manufacturing;
use App\Models\ApplicationCategories;
use App\Models\Contact;
use App\Models\ProductCover;
use App\Models\NewsCover;
use App\Models\News;

use App\Mail\ContactMaile;
use App\Mail\FoodMailer;
use App\Mail\SplytMailer;

use Session, Mail;
class HomeController extends Controller
{
    public function index()
    {
        $product = Product::limit(6)->get();
        $certification = Certification::all();
        $instagram = InstagramContent::all();
        $home = Home::first();
        $news = News::orderByDesc('id')->limit(5)->get();
        $data = [
            'product' => $product,
            'certification' => $certification,
            'instagram' => $instagram,
            'home' => $home,
            'news' => $news
        ];

        return view('pages.home.index')->with($data);
    }
    public function aboutus()
    {
        $certification = Certification::all();
        $about = AboutUs::first();
        $data = [
            'certification' => $certification,
            'about' => $about
        ];

        return view('pages.about.index')->with($data);
    }
    public function manufacturing()
    {
        $manufactur = Manufacturing::first();
        $data = [
            'manufactur' => $manufactur
        ];

        return view('pages.manufacturing.index')->with($data);
    }
    public function aseptic()
    {
        $category = ApplicationCategories::find(1);
        $aseptic = Application::where('application_category', 1)->get();
        $data = [
            'aseptic' => $aseptic,
            'category' => $category
        ];

        return view('pages.application.aseptic')->with($data);
    }
    public function paper()
    {
        $category = ApplicationCategories::find(2);
        $paper = Application::where('application_category', 2)->get();

        $data = [
            'paper' => $paper,
            'category' => $category
        ];

        return view('pages.application.paper')->with($data);
    }
    public function product()
    {
        $cover = ProductCover::first();
        $productSplyt = Product::where('product_category', 'Splyt-pak')->get();
        $productFood = Product::where('product_category', 'Food & Beverage')->get();
        $data = [
            'productSplyt' => $productSplyt,
            'productFood' => $productFood,
            'cover' => $cover
        ];

        return view('pages.product.index')->with($data);
    }
    public function product_json(Request $request){
        $id = $request->id;
        $data = Product::where('product_id', $id)->first();
        return response()->json(['message' => 'Data berhasil didapatkan', 'data' => $data], 200);
    }
    public function productFootmail(Request $request){
        $product_name = $request->product_name;
        $size = $request->size;
        $color = $request->color;
        $amount = $request->amount;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $company = $request->company;

        $mail = Mail::to($request->email)->send(new FoodMailer($product_name, $size, $color, $amount, $name, $email, $phone, $company));
        if($mail){
            return back()->with('success', 'success');
        } else {
            return back()->with('error', 'error');
        }
    }
    public function productSplytmail(Request $request){
        $product_name = $request->product_name;
        $size = $request->product_size;
        $amount = $request->amount;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $company = $request->company;

        $mail = Mail::to($request->email)->send(new SplytMailer($product_name, $size, $amount, $name, $email, $phone, $company));
        if($mail){
            return back()->with('success', 'success');
        } else {
            return back()->with('error', 'error');
        }
    }
    public function news()
    {
        $cover = NewsCover::first();
        $news = News::orderBy('news_created', 'DESC')->paginate(12);
        $data = [
            'cover' => $cover,
            'news' => $news
        ];

        return view('pages.news.index')->with($data);
    }
    public function news_detail($id, $slug)
    {
        $cover = NewsCover::first();
        $id = $id;
        $news = News::find($id);
        $news->update(['read_post' => intval($news->read_post) + 1]);
        if(!$news) abort(404);
        if(\Str::slug($news->title) !== $slug) abort(404);

        $data = [
            'cover' => $cover,
            'news' => $news
        ];

        return view('pages.news.detail')->with($data);
    }
    public function contact()
    {
        $contact = Contact::first();
        $data = [
            'contact' => $contact
        ];

        return view('pages.contact.index')->with($data);
    }
    public function contact_store(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'company' => 'required',
            'position' => 'required'
        ]);
        $contact = ContactUs::create([
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'email' => $request->email,
            'phone' => $request->telephone,
            'company' => $request->company,
            'position' => $request->position
        ]);

        $mail = Mail::to($request->email)->send(new ContactMaile($request->firstname, $request->lastname, $request->email, $request->telephone, $request->company, $request->position));
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Disimpan!',
            'data'    => $contact
        ]);
    }
}
