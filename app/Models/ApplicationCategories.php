<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationCategories extends Model
{
    use HasFactory;
    protected $table = 'application_category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'header_img',
        'title_heading',
        'desc_heading',
        'desc1_heading',
        'created_at',
        'updated_at'
    ];
}
