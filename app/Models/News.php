<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'feature_image',
        'author',
        'content',
        'news_created',
        'read_post',
        'created_at',
        'updated_at',
    ];
}
