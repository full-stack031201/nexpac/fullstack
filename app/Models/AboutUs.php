<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    use HasFactory;
    protected $table = 'about_us';
    protected $primaryKey = 'id';
    protected $fillable = [
        'section1_title',
        'section1_desc',
        'section1_desc1',
        'section1_img',
        'section2_title',
        'section2_desc',
        'section3_title',
        'section3_desc',
        'section4_title',
        'section4_desc',
        'section5_img',
        'created_at',
        'updated_at',
    ];
}
