<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufacturing extends Model
{
    use HasFactory;
    protected $table = 'manufacturing';
    protected $primaryKey = 'id';
    protected $fillable = [
        'header_img',
        'title_heading',
        'desc_heading',
        'section1_title',
        'section1_desc',
        'section1_img',
        'section2_title',
        'section2_desc',
        'section2_img',
        'section3_title',
        'section3_desc',
        'section3_img',
        'created_at',
        'updated_at',
    ];
}
