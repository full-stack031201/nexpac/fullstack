<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;
    protected $table = 'home';
    protected $primaryKey = 'id';
    protected $fillable = [
        'text1header',
        'text2header',
        'text3header',
        'imageheader',

        'titlesection2',
        'imgsec2_1',
        'textsec2_1',
        'imgsec2_2',
        'textsec2_2',
        'imgsec2_3',
        'textsec2_3',
        'imgsec2_4',
        'textsec2_4',

        'imgsec3',
        'titlesec3',
        'textsec3',

        'imgsec5',
        'titlesec5',
        'descsec5',

        'created_at',
        'updated_at',
    ];
}
