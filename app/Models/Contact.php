<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    protected $table = 'contact';
    protected $primaryKey = 'id';
    protected $fillable = [
        'header_img',
        'maps',
        'title_section1',
        'logo_sansico',
        'perusahaan',
        'alamat',
        'email',
        'telp',
        'fb',
        'ig',
        'linkedin',
        'created_at',
        'updated_at'
    ];
}
