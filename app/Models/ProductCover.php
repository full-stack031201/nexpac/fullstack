<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCover extends Model
{
    use HasFactory;
    protected $table = 'product_cover';
    protected $primaryKey = 'id';
    protected $fillable = [
        'header_img',
        'title_heading',
        'sub1',
        'sub2',
        'created_at',
        'updated_at',
    ];
}
