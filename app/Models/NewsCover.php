<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsCover extends Model
{
    use HasFactory;
    protected $table = 'news_cover';
    protected $primaryKey = 'id';
    protected $fillable = [
        'header_img',
        'title_heading',
        'created_at',
        'updated_at',
    ];
}
