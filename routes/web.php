<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\MasterLoginController;
use App\Http\Controllers\Admin\MasterApplicationController;
use App\Http\Controllers\Admin\MasterProductController;
use App\Http\Controllers\Admin\LayoutHomeController;
use App\Http\Controllers\Admin\LayoutAboutController;
use App\Http\Controllers\Admin\LayoutManufacturingController;
use App\Http\Controllers\Admin\LayoutNewsController;
use App\Http\Controllers\Admin\LayoutContactController;
use App\Http\Controllers\Admin\NewsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home');
    Route::get('/about-us', 'aboutus')->name('aboutus');
    Route::get('/manufacturing', 'manufacturing')->name('manufacturing');
    Route::get('/product', 'product')->name('product');
    Route::get('/product/json', 'product_json')->name('productJson');
    Route::post('/product/foodmail', 'productFootmail')->name('product.foodmail');
    Route::post('/product/splytmail', 'productSplytmail')->name('product.splytmail');
    Route::prefix('/applications')->group(function(){
        Route::get('/aseptic-packaging', 'aseptic')->name('aseptic');
        Route::get('/paper-packaging', 'paper')->name('paper');
    });
    Route::prefix('/contact-us')->group(function(){
        Route::get('/', 'contact')->name('contact');
        Route::post('/store', 'contact_store')->name('contact.store');
    });
    Route::prefix('/news')->group(function(){
        Route::get('/', 'news')->name('news');
        Route::get('/{id}/{slug}', 'news_detail')->name('news.detail');
    });
});
Route::get('/panel', function(){ return redirect()->route('login'); });
Route::prefix('/admin')->group(function(){
    Route::get('/', [AuthController::class, 'login'])->name('login');
    Route::post('/', [AuthController::class, 'login_process'])->name('login.process');
});
Route::get('/logout', [AuthController::class, 'logout_process'])->name('logout');

Route::middleware('auth')->group(function(){
    Route::controller(DashboardController::class)->group(function(){
        Route::prefix('/dashboard')->group(function(){
            Route::get('/', 'index')->name('dashboard');
        });
    });
    Route::prefix('/master')->name('master.')->group(function(){
        Route::resource('/login', MasterLoginController::class);
        Route::post('/login/{id}', [MasterLoginController::class, 'update']);

        Route::resource('/application', MasterApplicationController::class);
        Route::resource('/product', MasterProductController::class);
    });
    Route::prefix('/layouts')->name('layout.')->group(function(){
        Route::resource('/home', LayoutHomeController::class);
        Route::resource('/about', LayoutAboutController::class);
        Route::resource('/manufacturing', LayoutManufacturingController::class);
        Route::resource('/news', LayoutNewsController::class);
        Route::resource('/contact', LayoutContactController::class);
    });
    Route::prefix('/admin')->group(function(){
        Route::resource('/news', NewsController::class);
        Route::post('/news/{id}', [NewsController::class, 'update'])->name('news.update');
        Route::post('/news-upload-image', [NewsController::class, 'upload_image'])->name('news.upload.image');
    });
});
